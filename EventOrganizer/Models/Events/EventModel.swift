//
//  EventModel.swift
//  EventOrganizer
//
//  Created by Alexander Karaatanasov on 12.01.19.
//  Copyright © 2019 Alexander Karaatanassov. All rights reserved.
//

import Foundation
import ObjectMapper

class EventModel: ImmutableMappable, Equatable {
    
    let id: Int
    let clientId: Int? // make non-optional
    let organizerId: Int? // make non-optional
    let typeId: Int? // make non-optional
    let statusId: Int? // make non-optional
    let title: String
    let description: String
    let date: String
    let exactDate: String?
    let location: String?
    let feedingPrice: String?
    let accommodationPrice: String?
    let pricePerPerson: String?
    let involved: String
    let creationDate: String
    let lastUpdate: String
    let comments: [CommentModel]?
    
    required init(map: Map) throws {
        id = try map.value("id")
        clientId = try map.value("client_id")
        organizerId = try map.value("organizer_id")
        typeId = try map.value("type_id")
        statusId = try map.value("status_id")
        title = try map.value("title")
        description = try map.value("description")
        date = try map.value("date")
        exactDate = try map.value("exact_date")
        location = try map.value("location")
        feedingPrice = try map.value("feeding_price")
        accommodationPrice = try map.value("accommodation_price")
        pricePerPerson = try map.value("price_per_person")
        involved = try map.value("involved")
        creationDate = try map.value("created_at")
        lastUpdate = try map.value("updated_at")
        comments = try map.value("comments")
    }
    
    func mapping(map: Map) {
        id >>> map["id"]
        clientId >>> map["client_id"]
        organizerId >>> map["organizer_id"]
        typeId >>> map["type_id"]
        statusId >>> map["status_id"]
        title >>> map["title"]
        description >>> map["description"]
        date >>> map["date"]
        exactDate >>> map["exact_date"]
        location >>> map["location"]
        feedingPrice >>> map["feeding_price"]
        accommodationPrice >>> map["accommodation_price"]
        pricePerPerson >>> map["price_per_person"]
        involved >>> map["involved"]
        creationDate >>> map["created_at"]
        lastUpdate >>> map["updated_at"]
        comments >>> map["comments"]
    }
    
    init(id: Int, clientId: Int, organizerId: Int, typeId: Int, statusId: Int, title: String, description: String,
         date: String, exactDate: String, location: String, feedingPrice: String, accommodationPrice: String, pricePerPerson: String, involved: String,
         creationDate: String, lastUpdate: String, comments: [CommentModel]) {
        self.id = id
        self.clientId = clientId
        self.organizerId = organizerId
        self.typeId = typeId
        self.statusId = statusId
        self.title = title
        self.description = description
        self.date = date
        self.exactDate = exactDate
        self.location = location
        self.feedingPrice = feedingPrice
        self.accommodationPrice = accommodationPrice
        self.pricePerPerson = pricePerPerson
        self.involved = involved
        self.creationDate = creationDate
        self.lastUpdate = lastUpdate
        self.comments = comments
    }
    
    static func == (lhs: EventModel, rhs: EventModel) -> Bool {
        return lhs.id == rhs.id
    }
}
