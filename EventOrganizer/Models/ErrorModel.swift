//
//  ErrorModel.swift
//  EventOrganizer
//
//  Created by Alexander Karaatanasov on 12.01.19.
//  Copyright © 2019 Alexander Karaatanassov. All rights reserved.
//

import Foundation
import ObjectMapper

enum ErrorType: String {
    case unauthorized = "token_not_provided"
    case tokenExpired = "token_expired"
    // add other errors
}

class ErrorModel: ImmutableMappable, Error {
    let error: String
    
    required init(_ error: Error) {
        self.error = ""
    }
    
    convenience init(error: String) {
        self.init(NSError(domain: "", code: 0, userInfo: [NSLocalizedDescriptionKey: error]))
    }
    
    required init(map: Map) throws {
        error = try map.value("error")
    }
    
    func mapping(map: Map) {
        error >>> map["error"]
    }
}
