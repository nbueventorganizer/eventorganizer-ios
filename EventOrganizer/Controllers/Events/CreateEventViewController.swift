//
//  CreateEventViewController.swift
//  EventOrganizer
//
//  Created by Alexander Karaatanasov on 13.01.19.
//  Copyright © 2019 Alexander Karaatanassov. All rights reserved.
//

import UIKit
import Sheeeeeeeeet
import SVProgressHUD

class CreateEventTableViewController: InputTableViewController {
    
    // MARK: - Vars
    
    private var eventType: EventsType?
    
    private var types: [TypeModel] = []
    
    private var organizers: [OrganizerModel] = []
    
    private var eventId: Int?
    private var selectedTypeId: Int?
    private var selectedOrganizerId: Int?
    private var createdEvent: CreateEventModel?
    
    private var eventTitle: String?
    private var eventTypeId: Int?
    private var eventOrganizerId: Int?
    private var eventDescription: String?
    private var eventDate: String?
    private var eventInvolved: String?
    
    // MARK: - IBOutlets
    
    @IBOutlet weak var titleTextField: FloatingTextField!
    @IBOutlet weak var typeTextField: FloatingTextField!
    @IBOutlet weak var organizerTextField: FloatingTextField!
    @IBOutlet weak var descriptionTextField: FloatingTextField!
    @IBOutlet weak var dateTextField: FloatingTextField!
    @IBOutlet weak var involvedTextField: FloatingTextField!
    
    @IBOutlet weak var exactDateTextField: FloatingTextField!
    @IBOutlet weak var locationTextField: FloatingTextField!
    @IBOutlet weak var feedingPriceTextField: FloatingTextField!
    @IBOutlet weak var accommodationPriceTextField: FloatingTextField!
    @IBOutlet weak var pricePerPersonTextField: FloatingTextField!
    
    
    // MARK: - Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        view.endEditing(true)
    }
    
    // MARK: - Public
    
    func configure(with eventType: EventsType, types: [TypeModel], organizers: [OrganizerModel]) {
        self.eventType = eventType
        self.types = types
        self.organizers = organizers
    }
    
    func initializeCreateEvent(with event: EventModel) {
        eventId = event.id
        eventTitle = event.title
        eventTypeId = event.typeId
        eventOrganizerId = event.organizerId
        eventDescription = event.description
        eventDate = event.date
        eventInvolved = event.involved
    }
    
    // MARK: - Private
    
    private func setupUI() {
        setupDatePicker()
        loadTextFields()
    }
    
    private func loadTextFields() {
        if let title = eventTitle {
            titleTextField.text = title
        }
        
        if let typeId = eventTypeId {
            types.forEach {
                if $0.id == typeId {
                    typeTextField.text = $0.title
                }
            }
        }
        
        if let organizerId = eventOrganizerId {
            organizers.forEach {
                if $0.id == organizerId {
                    organizerTextField.text = $0.name
                }
            }
        }
        
        if let description = eventDescription {
            descriptionTextField.text = description
        }
        
        if let date = eventDate {
            dateTextField.text = date
        }
            
        if let involved = eventInvolved {
            involvedTextField.text = involved
        }
    }
    
    private func setupDatePicker() {
        let datePicker = UIDatePicker()
        datePicker.datePickerMode = .date
        datePicker.addTarget(self, action: #selector(self.dateChanged(datePicker:)), for: .valueChanged)
        
        let exactDatePicker = UIDatePicker()
        exactDatePicker.datePickerMode = .date
        exactDatePicker.addTarget(self, action: #selector(self.exactDateChanged(datePicker:)), for: .valueChanged)
        
        dateTextField.inputView = datePicker
        exactDateTextField.inputView = exactDatePicker
    }
    
    @objc private func dateChanged(datePicker: UIDatePicker) {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        dateTextField.text = dateFormatter.string(from: datePicker.date)
        view.endEditing(true)
    }
    
    @objc private func exactDateChanged(datePicker: UIDatePicker) {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        exactDateTextField.text = dateFormatter.string(from: datePicker.date)
        view.endEditing(true)
    }
    
    private func presentTypesActionSheet(from view: UIView?) {
        let sheet = StandardActionSheet(options: types, action: setSelectedType)
//        sheet.presenter.events.didDismissWithBackgroundTap = { print("Background tap!") }
        sheet.present(in: self, from: view)
    }
    
    private func presentOrganizersActionSheet(from view: UIView?) {
        let sheet = StandardActionSheet(options: organizers, action: setSelectedOrganizer)
//        sheet.presenter.events.didDismissWithBackgroundTap = { print("Background tap!") }
        sheet.present(in: self, from: view)
    }
    
    private func setSelectedType(typeId: Int, selectedItem: ActionSheetItem) {
        let selectedType = selectedItem.title
        typeTextField.text = selectedType
        selectedTypeId = typeId
    }
    
    private func setSelectedOrganizer(organizerId: Int, selectedItem: ActionSheetItem) {
        let selectedOrganizer = selectedItem.title
        organizerTextField.text = selectedOrganizer
        selectedOrganizerId = organizerId
    }
    
    @objc private func dismissAndPopController() {
        dismiss(animated: true) { [weak self] in
            if let navigationController = self?.navigationController {
                navigationController.popViewController(animated: true)
                if let eventTableViewController = navigationController.viewControllers.first as? EventsTableViewController {
                    eventTableViewController.reloadData()
                }
            }
        }
    }
    
    // MARK: - Requests
    
    private func sendCreatedEvent(with model: CreateEventModel?) {
        SVProgressHUD.show()
        if eventType == .organizerEvents {
            guard let event = createdEvent, let id = eventId else { return }
            PrivateManagerAPI.sharedInstance.acceptEvent(with: event, and: id, completionHandler: { (response, error) in
                SVProgressHUD.dismiss()
                if let errorModel = error {
                    switch errorModel.error {
                    case ErrorType.unauthorized.rawValue:
                        AlertPresenter.showInformationAlert(from: self, title: "Oops", message: "You must login before you can create events.")
                    case ErrorType.unauthorized.rawValue:
                        AlertPresenter.showInformationAlert(from: self, title: "Oops", message: "Session expired. Please log in again.")
                    default: break
                    }
                } else {
                    AlertPresenter.showInformationAlert(from: self,
                                                        title: "Success",
                                                        message: "This event was accepted successfully.",
                                                        okAction: (target: self, actionSelector: #selector(self.dismissAndPopController)))
                }
            })
        } else {
            guard let event = createdEvent else { return }
            PrivateManagerAPI.sharedInstance.sendCreateEvent(with: event, completionHandler: { (response, error) in
                SVProgressHUD.dismiss()
                if let responseModel = response {
                    if responseModel.success {
                        AlertPresenter.showInformationAlert(from: self,
                                                            title: "Success",
                                                            message: "Your event was submitted successfully.",
                                                            okAction: (target: self, actionSelector: #selector(self.dismissAndPopController)))
                    } else {
                        AlertPresenter.showInformationAlert(from: self,
                                                            title: "Oops",
                                                            message: "Something went wrong. Please try again!")
                    }
                } else if let errorModel = error {
                    switch errorModel.error {
                    case ErrorType.unauthorized.rawValue:
                        AlertPresenter.showInformationAlert(from: self, title: "Oops", message: "You must login before you can create events.")
                    case ErrorType.unauthorized.rawValue:
                        AlertPresenter.showInformationAlert(from: self, title: "Oops", message: "Session expired. Please log in again.")
                    default: break
                    }
                }
            })
        }
        
    }
    
    // MARK: - IBAction
    
    @IBAction func createEvent(_ sender: UIButton) {
        guard let eventType = eventType else {
            print("NO EVENT TYPE ?!?")
            return
        }
        
        if eventType == .allEvents || eventType == .clientEvents {
            guard let title = titleTextField.text, !title.isEmpty else {
                AlertPresenter.showInformationAlert(from: self, title: "Oops", message: "Enter title")
                return
            }
            guard let typeId = selectedTypeId else {
                AlertPresenter.showInformationAlert(from: self, title: "Oops", message: "Select a type")
                return
            }
            guard let organizerId = selectedOrganizerId else {
                AlertPresenter.showInformationAlert(from: self, title: "Oops", message: "Select an organizer")
                return
            }
            guard let description = descriptionTextField.text, !description.isEmpty else {
                AlertPresenter.showInformationAlert(from: self, title: "Oops", message: "Enter description")
                return
            }
            guard let date = dateTextField.text, !date.isEmpty else {
                AlertPresenter.showInformationAlert(from: self, title: "Oops", message: "Enter date")
                return
            }
            guard let involved = involvedTextField.text, !date.isEmpty else {
                AlertPresenter.showInformationAlert(from: self, title: "Oops", message: "Enter involvement")
                return
            }
            
            createdEvent = CreateEventModel(title: title,
                                            organizerId: organizerId,
                                            typeId: typeId,
                                            description: description,
                                            date: date,
                                            involved: involved)
        } else {
            guard let exactDate = exactDateTextField.text, !exactDate.isEmpty else {
                AlertPresenter.showInformationAlert(from: self, title: "Oops", message: "Enter exact date")
                return
            }
            guard let location = locationTextField.text, !location.isEmpty else {
                AlertPresenter.showInformationAlert(from: self, title: "Oops", message: "Enter location")
                return
            }
            guard let feedingPrice = feedingPriceTextField.text, !feedingPrice.isEmpty else {
                AlertPresenter.showInformationAlert(from: self, title: "Oops", message: "Enter feeding price")
                return
            }
            guard let accommodationPrice = accommodationPriceTextField.text, !accommodationPrice.isEmpty else {
                AlertPresenter.showInformationAlert(from: self, title: "Oops", message: "Enter accommodation price")
                return
            }
            guard let pricePerPerson = pricePerPersonTextField.text, !pricePerPerson.isEmpty else {
                AlertPresenter.showInformationAlert(from: self, title: "Oops", message: "Enter price per person")
                return
            }
            
            createdEvent = CreateEventModel(exactDate: exactDate,
                                            location: location,
                                            feedingPrice: feedingPrice.toNumber(),
                                            accommodationPrice: accommodationPrice.toNumber(),
                                            pricePerPerson: pricePerPerson.toNumber())
        }

        sendCreatedEvent(with: createdEvent)
    }
    
    @IBAction func tap(_ sender: UITapGestureRecognizer) {
        tableView.endEditing(true)
    }
    
}

extension CreateEventTableViewController {
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.row == 1 { presentTypesActionSheet(from: tableView) } // present type picker
        if indexPath.row == 2 { presentOrganizersActionSheet(from: tableView) } // present organizer picker
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return eventType == .organizerEvents ? 11 : 6
    }
}
