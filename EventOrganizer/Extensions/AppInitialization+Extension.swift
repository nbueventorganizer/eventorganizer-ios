//
//  AppInitialization+Extension.swift
//  EventOrganizer
//
//  Created by Alexander Karaatanasov on 1.12.18.
//  Copyright © 2018 Alexander Karaatanassov. All rights reserved.
//

import Foundation
import XCGLogger
import SVProgressHUD

let logger = XCGLogger.default

extension AppDelegate {
    
    func setupLogger() {
        #if DEBUG
        logger.setup(level: .verbose,
                     showLogIdentifier: false,
                     showFunctionName: true,
                     showThreadName: false,
                     showLevel: false,
                     showFileNames: true,
                     showLineNumbers: true,
                     showDate: true,
                     writeToFile: nil,
                     fileLevel: .verbose)
        #endif
    }
    
    func setupVGProgressHUD() {
        SVProgressHUD.setDefaultMaskType(.black)
        SVProgressHUD.setBackgroundColor(.white)
    }
    
    func setInitialScreen() {
        if UserAuthentication.userIsLoggedIn() {
            ControllerManager.switchRootController(to: .home)
        } else {
            ControllerManager.switchRootController(to: .login)
        }
    }
    
    func clearKeychainIfNeeded() { // Example how to use UserDefaults
        let userDefaults = UserDefaults.standard
        
        if !userDefaults.bool(forKey: "hasRunBefore") {
            do {
                try KeychainAccess.sharedInstance.keychain.removeAll()
            } catch let error {
                logger.error(error)
            }
            userDefaults.set(true, forKey: "hasRunBefore")
        }
    }
    
}

