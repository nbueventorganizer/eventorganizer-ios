//
//  OrganizerModel.swift
//  EventOrganizer
//
//  Created by Alexander Karaatanasov on 13.01.19.
//  Copyright © 2019 Alexander Karaatanassov. All rights reserved.
//

import Foundation
import ObjectMapper
import Sheeeeeeeeet

class OrganizerModel: ImmutableMappable {
    // - TODO: Add all properties
    var id: Int
    var name: String
    var email: String
    
    required init(map: Map) throws {
        id = try map.value("id")
        name = try map.value("name")
        email = try map.value("email")
    }
    
    func mapping(map: Map) {
        id <- map["id"]
        name <- map["name"]
        email <- map["email"]
    }
    
    init(id: Int, name: String, email: String) {
        self.id = id
        self.name = name
        self.email = email
    }
}

extension OrganizerModel {
    func item() -> ActionSheetItem {
        return ActionSheetItem(title: name)
    }
}
