//
//  String+Extension.swift
//  EventOrganizer
//
//  Created by Alexander Karaatanasov on 27.01.19.
//  Copyright © 2019 Alexander Karaatanassov. All rights reserved.
//

import Foundation

extension String {
    func toNumber() -> Double {
        if let result = Double(self) {
            return result
        } else {
            return Double(self.replacingOccurrences(of: ",", with: "."))!
        }
        
    }
}
