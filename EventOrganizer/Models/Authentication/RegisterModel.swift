//
//  RegisterModel.swift
//  EventOrganizer
//
//  Created by Alexander Karaatanasov on 13.01.19.
//  Copyright © 2019 Alexander Karaatanassov. All rights reserved.
//

import Foundation
import ObjectMapper

class RegisterModel: ImmutableMappable {
    var name: String
    var email: String
    var password: String
    var organizer: Bool
    
    required init(map: Map) throws {
        name = try map.value("name")
        email = try map.value("email")
        password = try map.value("password")
        organizer = try map.value("organizer")
    }
    
    func mapping(map: Map) {
        name <- map["name"]
        email <- map["email"]
        password <- map["password"]
        organizer <- map["organizer"]
    }
    
    init(name: String, email: String, password: String, role: String) {
        self.name = name
        self.email = email
        self.password = password
        self.organizer = (role != "Client")
    }
}
