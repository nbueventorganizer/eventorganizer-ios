//
//  ProfileViewController.swift
//  EventOrganizer
//
//  Created by Alexander Karaatanasov on 13.01.19.
//  Copyright © 2019 Alexander Karaatanassov. All rights reserved.
//

import UIKit
import SVProgressHUD

class ProfileViewController: UIViewController {
    
    // MARK: - Vars
    
    private var currentUserIsOrganizer: Bool {
        return checkIfOrganizer()
    }
    
    private var organizers: [OrganizerModel] = []

    // MARK: - IBOutlet
    
    @IBOutlet weak var userEvents: UIButton!
    
    // MARK: - Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        loadData()
    }
    
    // MARK: - Navigation
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        switch segue.identifier {
        case "showMyEvents":
            if let eventsController = segue.destination as? EventsTableViewController {
                if UserAuthentication.userIsOrganizer() {
                    eventsController.setScreenType(for: .organizerEvents)
                } else {
                    eventsController.setScreenType(for: .clientEvents)
                }
            }
        default:
            break
        }
    }
    
    // MARK: - Private
    
    private func setupUI() {
        print("UserProfile")
        print("Organizer: \(currentUserIsOrganizer)")
        
        if UserAuthentication.isOrganizerNotSet() {
            UserAuthentication.isOrganizerSet(flag: currentUserIsOrganizer)
        }
        
        if UserAuthentication.userIsOrganizer() {
           userEvents.titleLabel?.text = "Pending Events"
        }
    }
    
    private func checkIfOrganizer() -> Bool {
        var result = false
        let currentUserEmail = UserAuthentication.getEmail()
        organizers.forEach {
            if $0.email == currentUserEmail {
                result = true
            }
        }
        return result
    }
    
    // MARK: - Requests
    
    private func loadData() {
        SVProgressHUD.show()
        loadOrganizers { [weak self] organizers in
            SVProgressHUD.dismiss()
            self?.organizers = organizers
            self?.setupUI()
        }
    }
    
    private func loadOrganizers(completionHandler: @escaping (([OrganizerModel]) -> Void)) {
        PublicManagerAPI.sharedInstance.getOrganizers { [weak self] (organizers, error) in
            if let response = organizers {
                completionHandler(response.organizers)
            } else if let error = error, let controller = self {
                AlertPresenter.showInformationAlert(from: controller, title: "Sorry", message: error.localizedDescription)
            }
        }
    }
    
    // MARK: - IBAction
    
    @IBAction func showMyEvents(_ sender: Any) {
        performSegue(withIdentifier: "showMyEvents", sender: self)
    }
    
    @IBAction func logout(_ sender: Any) {
        UserAuthentication.logout()
        ControllerManager.switchRootController(to: .login)
    }

}
