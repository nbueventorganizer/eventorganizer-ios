//
//  RoundedButton.swift
//  EventOrganizer
//
//  Created by Alexander Karaatanasov on 30.11.18.
//  Copyright © 2018 Alexander Karaatanassov. All rights reserved.
//

import Foundation
import UIKit

@IBDesignable
public class RoundedButton: UIButton {
    override public init(frame: CGRect) {
        super.init(frame: frame)
        setupCornerRadius()
    }

    required public init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setupCornerRadius()
    }

    // MARK: Private

    private func setupCornerRadius() {
        layer.cornerRadius = bounds.height/2
    }
}
