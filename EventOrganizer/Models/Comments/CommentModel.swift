//
//  CommentModel.swift
//  EventOrganizer
//
//  Created by Alexander Karaatanasov on 27.01.19.
//  Copyright © 2019 Alexander Karaatanassov. All rights reserved.
//

import Foundation
import ObjectMapper

class CommentModel: ImmutableMappable {
    
    var id: Int
    var eventId: Int
    var userId: Int
    var text: String
    var creationDate: String
    var lastUpdate: String
    
    required init(map: Map) throws {
        id = try map.value("id")
        eventId = try map.value("event_id")
        userId = try map.value("user_id")
        text = try map.value("text")
        creationDate = try map.value("created_at")
        lastUpdate = try map.value("updated_at")
    }
    
    func mapping(map: Map) {
        id <- map["id"]
        eventId <- map["event_id"]
        userId <- map["user_id"]
        text <- map["text"]
        creationDate <- map["created_at"]
        lastUpdate <- map["updated_at"]
    }
    
    init(id: Int, eventId: Int, userId: Int, text: String, creationDate: String, lastUpdate: String) {
        self.id = id
        self.eventId = eventId
        self.userId = userId
        self.text = text
        self.creationDate = creationDate
        self.lastUpdate = lastUpdate
    }
    
}
