//
//  GalleryDetailViewController.swift
//  EventOrganizer
//
//  Created by Alexander Karaatanasov on 28.01.19.
//  Copyright © 2019 Alexander Karaatanassov. All rights reserved.
//

import UIKit
import PreviewTransition

public class GalleryDetailViewController: PTDetailViewController {
    
    @IBOutlet var controlBottomConstrant: NSLayoutConstraint!
    
    // bottom control icons
    @IBOutlet var controlsViewContainer: UIView!
    @IBOutlet var controlView: UIView!
    @IBOutlet var plusImageView: UIImageView!
    @IBOutlet var controlTextLabel: UILabel!
    @IBOutlet var controlTextLableLending: NSLayoutConstraint!
    @IBOutlet var shareImageView: UIImageView!
    @IBOutlet var hertIconView: UIImageView!
    
    var backButton: UIButton?
    
    var bottomSafeArea: CGFloat {
        var result: CGFloat = 0
        if #available(iOS 11.0, *) {
            result = UIApplication.shared.keyWindow?.safeAreaInsets.bottom ?? 0
        }
        return result
    }
}

// MARK: life cicle

extension GalleryDetailViewController {
    
    override public func viewDidLoad() {
        super.viewDidLoad()
        
        backButton = createBackButton()
        _ = createNavigationBarBackItem(button: backButton)

        // animations
        showBackButtonDuration(duration: 0.4)
        showControlViewDuration(duration: 0.4)
        
        _ = createBlurView()
    }
    
}

// MARK: helpers

extension GalleryDetailViewController {
    
    fileprivate func createBackButton() -> UIButton {
        let button = UIButton(frame: CGRect(x: 0, y: 0, width: 22, height: 20))
        button.setImage(UIImage.Asset.Back.image, for: .normal)
        button.addTarget(self, action: #selector(GalleryDetailViewController.backButtonHandler), for: .touchUpInside)
        return button
    }
    
    fileprivate func createNavigationBarBackItem(button: UIButton?) -> UIBarButtonItem? {
        guard let button = button else {
            return nil
        }
        
        let buttonItem = UIBarButtonItem(customView: button)
        navigationItem.leftBarButtonItem = buttonItem
        return buttonItem
    }
    
    fileprivate func createBlurView() -> UIView {
        let height = controlView.bounds.height + bottomSafeArea
        let imageFrame = CGRect(x: 0, y: view.frame.size.height - height, width: view.frame.width, height: height)
        let image = view.makeScreenShotFromFrame(frame: imageFrame)
        let screnShotImageView = UIImageView(image: image)
        screnShotImageView.blurViewValue(value: 5)
        screnShotImageView.frame = controlsViewContainer.bounds
        screnShotImageView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        controlsViewContainer.insertSubview(screnShotImageView, at: 0)
        addOverlay(toView: screnShotImageView)
        return screnShotImageView
    }
    
    fileprivate func addOverlay(toView view: UIView) {
        let overlayView = UIView(frame: view.bounds)
        overlayView.backgroundColor = .black
        overlayView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        overlayView.alpha = 0.4
        view.addSubview(overlayView)
    }
}

// MARK: animations

extension GalleryDetailViewController {
    
    fileprivate func showBackButtonDuration(duration: Double) {
        backButton?.rotateDuration(duration: duration, from: -CGFloat.pi / 4, to: 0)
        backButton?.scaleDuration(duration: duration, from: 0.5, to: 1)
        backButton?.opacityDuration(duration: duration, from: 0, to: 1)
    }
    
    fileprivate func showControlViewDuration(duration: Double) {
        moveUpControllerDuration(duration: duration)
        showControlButtonsDuration(duration: duration)
        showControlLabelDuration(duration: duration)
    }
    
    fileprivate func moveUpControllerDuration(duration: Double) {
        
        controlBottomConstrant.constant = -controlsViewContainer.bounds.height
        view.layoutIfNeeded()
        
        controlBottomConstrant.constant = 0
        UIView.animate(withDuration: duration, delay: 0, options: .curveEaseOut, animations: {
            self.view.layoutIfNeeded()
        }, completion: nil)
    }
    
    fileprivate func showControlButtonsDuration(duration: Double) {
        [plusImageView, shareImageView, hertIconView].forEach {
            $0?.rotateDuration(duration: duration, from: CGFloat.pi / 4, to: 0, delay: duration)
            $0?.scaleDuration(duration: duration, from: 0.5, to: 1, delay: duration)
            $0?.alpha = 0
            $0?.opacityDuration(duration: duration, from: 0, to: 1, delay: duration, remove: false)
        }
    }
    
    fileprivate func showControlLabelDuration(duration: Double) {
        controlTextLabel.alpha = 0
        controlTextLabel.opacityDuration(duration: duration, from: 0, to: 1, delay: duration, remove: false)
        
        // move rigth
        let offSet: CGFloat = 20
        controlTextLableLending.constant -= offSet
        view.layoutIfNeeded()
        
        controlTextLableLending.constant += offSet
        UIView.animate(withDuration: duration * 2, delay: 0, options: .curveEaseOut, animations: {
            self.view.layoutIfNeeded()
        }, completion: nil)
    }
}

// MARK: actions

extension GalleryDetailViewController {
    
    @objc func backButtonHandler() {
        popViewController()
    }
}

extension GalleryDetailViewController {
    fileprivate func configureNavigationBar() {
        //transparent background
        UINavigationBar.appearance().setBackgroundImage(UIImage(), for: .default)
        UINavigationBar.appearance().shadowImage = UIImage()
        UINavigationBar.appearance().isTranslucent = true
        
        if let font = UIFont(name: "Avenir-medium", size: 18) {
            UINavigationBar.appearance().titleTextAttributes = [
                NSAttributedString.Key.foregroundColor: UIColor.white,
                NSAttributedString.Key.font: font,
            ]
        }
    }
}

extension UIImage {
    enum Asset: String {
        case _1 = "1"
        case _2 = "2"
        case _3 = "3"
        case _4 = "4"
        case _5 = "5"
        case Back = "back"
        case HertIcon
        case PlusIcon
        case ShareIcon
        case TransparentPixel
        
        var image: UIImage {
            return UIImage(asset: self)
        }
    }
    
    convenience init!(asset: Asset) {
        self.init(named: asset.rawValue)
    }
}
