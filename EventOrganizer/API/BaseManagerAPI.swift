//
//  BaseManagerAPI.swift
//  EventOrganizer
//
//  Created by Alexander Karaatanasov on 12.01.19.
//  Copyright © 2019 Alexander Karaatanassov. All rights reserved.
//

import Foundation
import ObjectMapper
import Alamofire
import AlamofireObjectMapper
import SwiftyUserDefaults

open class BaseManagerAPI {
    
    var sessionManager: SessionManager = SessionManager()
    
    init() {
        let configuration = URLSessionConfiguration.default
        configuration.httpAdditionalHeaders = SessionManager.defaultHTTPHeaders
        
        sessionManager = SessionManager(configuration: configuration)
    }
    
    @discardableResult func performObjectRequest<T: ImmutableMappable>(_ URLRequest: URLRequestConvertible, completionHandler: @escaping (T?, ErrorModel?) -> Void) -> Request {
        let req = sessionManager.request(URLRequest)
            .validate()
            .responseObject { (dataResponse: DataResponse<T>) in
                switch dataResponse.result {
                case .success(let result):
                    completionHandler(result, nil)
                case .failure(let error):
                    logger.debug(error)
                    completionHandler(nil, self.parsedErrorCreate(error, data: dataResponse.data!))
                }
        }
        
        logger.verbose(req.debugDescription)
        return req
    }
    
//    @discardableResult func performLoginRequest<T: ImmutableMappable>(_ URLRequest: URLRequestConvertible, completionHandler: @escaping (T?, ErrorLoginModel?) -> Void) -> Request {
//        let req = sessionManager.request(URLRequest)
//            .validate()
//            .responseObject { (dataResponse: DataResponse<T>) in
//                switch dataResponse.result {
//                case .success(let result):
//                    completionHandler(result, nil)
//                case .failure(let error):
//                    Log.debug(error)
//                    completionHandler(nil, self.parsedLoginErrorCreate(error, data: dataResponse.data!))
//                }
//        }
//        
//        Log.verbose(req.debugDescription)
//        return req
//    }
    
    func performObjectRequestForRefresh<T: ImmutableMappable>(_ URLRequest: URLRequestConvertible, completionHandler: @escaping (T?, ErrorModel?) -> Void) {
        let req = sessionManager.request(URLRequest)
            .validate()
            .responseObject { (dataResponse: DataResponse<T>) in
                switch dataResponse.result {
                case .success(let result):
                    completionHandler(result, nil)
                case .failure(let error):
                    logger.debug(error)
                    completionHandler(nil, self.parsedErrorCreate(error, data: dataResponse.data!))
                }
        }
        
        logger.verbose(req.debugDescription)
    }
    
    @discardableResult func performArrayRequest<T: ImmutableMappable>(_ URLRequest: URLRequestConvertible, completionHandler: @escaping ([T]?, ErrorModel?) -> Void) -> DataRequest {
        let req = sessionManager.request(URLRequest)
            .validate()
            .responseArray { (dataResponse: DataResponse<[T]>) in
                switch dataResponse.result {
                case .success(let result):
                    completionHandler(result, nil)
                case .failure(let error):
                    logger.debug(error)
                    completionHandler([], self.parsedErrorCreate(error, data: dataResponse.data!))
                }
        }
        logger.verbose(req.debugDescription)
        return req
    }
    
    @discardableResult func performRequest(_ URLRequest: URLRequestConvertible, completionHandler: ((ErrorModel?) -> Void)? = nil) -> DataRequest {
        let req = sessionManager.request(URLRequest)
            .validate()
            .response { (dataResponse) in
                if let error = dataResponse.error {
                    completionHandler?(self.parsedErrorCreate(error, data: dataResponse.data ?? Data()))
                } else {
                    completionHandler?(nil)
                }
        }
        
        logger.verbose(req.debugDescription)
        return req
    }
    
    func performDataRequest(_ URLRequest: URLRequestConvertible, completionHandler: @escaping (DataResponse<Data>?, ErrorModel?) -> Void) {
        let req = sessionManager.request(URLRequest)
            .validate()
            .responseData { (dataResponse: DataResponse<Data>) in
                if let error = dataResponse.error {
                    completionHandler(nil, self.parsedErrorCreate(error, data: dataResponse.data ?? Data() ) )
                } else {
                    completionHandler(dataResponse, nil)
                }
        }
        
        logger.verbose(req.debugDescription)
    }
    
    func performHTMLRequest(_ URLRequest: URLRequestConvertible, completionHandler: @escaping (DataResponse<String>?, ErrorModel?) -> Void) {
        let req = sessionManager.request(URLRequest)
            .validate()
            .responseString { (dataResponse: DataResponse<String>) in
                if let error = dataResponse.error {
                    completionHandler(nil, self.parsedErrorCreate(error, data: dataResponse.data ?? Data() ) )
                } else {
                    completionHandler(dataResponse, nil)
                }
        }
        
        logger.verbose(req.debugDescription)
    }
    
    func parsedErrorCreate(_ failureError: Error, data: Data) -> ErrorModel {
        var parsedError: ErrorModel
        do {
            let jsonObject = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? NSDictionary
            parsedError = Mapper<ErrorModel>().map(JSONObject: jsonObject) ?? ErrorModel(failureError)
        }
        catch {
            parsedError = ErrorModel(failureError)
        }
        
        return parsedError
    }
    
//    func parsedLoginErrorCreate(_ failureError: Error, data: Data) -> ErrorLoginModel {
//        var parsedError: ErrorLoginModel
//        do {
//            let jsonObject = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? NSDictionary
//            parsedError = Mapper<ErrorLoginModel>().map(JSONObject: jsonObject) ?? ErrorLoginModel(failureError)
//        }
//        catch {
//            parsedError = ErrorLoginModel(failureError)
//        }
//
//        return parsedError
//    }
}
