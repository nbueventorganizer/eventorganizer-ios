//
//  KeychainKey.swift
//  EventOrganizer
//
//  Created by Alexander Karaatanasov on 1.12.18.
//  Copyright © 2018 Alexander Karaatanassov. All rights reserved.
//

import Foundation

struct KeychainKey {
    static let accessToken = "accessToken"
    static let service = "EventOrganizer"
}
