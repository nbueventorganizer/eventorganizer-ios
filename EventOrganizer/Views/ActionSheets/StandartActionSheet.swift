//
//  StandartActionSheet.swift
//  EventOrganizer
//
//  Created by Alexander Karaatanasov on 14.01.19.
//  Copyright © 2019 Alexander Karaatanassov. All rights reserved.
//

import Sheeeeeeeeet

enum RoleOption: String {
    case client
    case organizer
    
    var displayName: String {
        switch self {
        case .client: return "Client"
        case .organizer: return "Organizer"
        }
    }
}

extension RoleOption {
    func item() -> ActionSheetItem {
        return ActionSheetItem(title: displayName)
    }
}

class StandardActionSheet: ActionSheet {
    // Register
    init(options: [RoleOption], action: @escaping ([ActionSheetItem]) -> ()) {
        let items = StandardActionSheet.items(for: options)
        super.init(items: items) { _, item in
            action([item])
        }
    }
    // Create Event
    init(options: [TypeModel], action: @escaping (Int, ActionSheetItem) -> ()) {
        let items = StandardActionSheet.items(for: options)
        super.init(items: items) { sheet, selectedItem in
            var selectedItemIndex = 0
            for (index, currentItem) in sheet.items.enumerated() {
                if currentItem == selectedItem {
                    selectedItemIndex = index
                }
            }
            let selectedItemId = options[selectedItemIndex - 1].id // - 1 cause first item is not ActionSheetItem (it is ActionSheetTitle)
            action(selectedItemId, selectedItem)
        }
    }
    
    init(options: [OrganizerModel], action: @escaping (Int, ActionSheetItem) -> ()) {
        let items = StandardActionSheet.items(for: options)
        super.init(items: items) { sheet, selectedItem in
            var selectedItemIndex = 0
            for (index, currentItem) in sheet.items.enumerated() {
                if currentItem == selectedItem {
                    selectedItemIndex = index
                }
            }
            let selectedItemOptionId = selectedItemIndex - 1 < 0 ? 0 : selectedItemIndex - 1
            let selectedItemId = options[selectedItemOptionId].id // - 1 cause first item is not ActionSheetItem (it is ActionSheetTitle)
            action(selectedItemId, selectedItem)
        }
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
}

private extension StandardActionSheet {
    static func items(for options: [RoleOption]) -> [ActionSheetItem] {
        var items = options.map { $0.item() }
        items.insert(ActionSheetTitle(title: "Select your role"), at: 0)
//        items.append(ActionSheetCancelButton(title: "Cancel"))
        return items
    }
    
    static func items(for options: [TypeModel]) -> [ActionSheetItem] {
        var items = options.map { $0.item() }
        items.insert(ActionSheetTitle(title: "Select event type"), at: 0)
        return items
    }
    
    static func items(for options: [OrganizerModel]) -> [ActionSheetItem] {
        var items = options.map { $0.item() }
        items.insert(ActionSheetTitle(title: "Select your organizer"), at: 0)
        return items
    }
}
