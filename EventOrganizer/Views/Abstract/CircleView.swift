//
//  CircleView.swift
//  EventOrganizer
//
//  Created by Alexander Karaatanasov on 30.11.18.
//  Copyright © 2018 Alexander Karaatanassov. All rights reserved.
//

import UIKit

public class CircleView: UIView {

    // MARK: - Lifecycle

    override public init(frame: CGRect) {
        super.init(frame: frame)
        setupView()
    }

    required public init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setupView()
    }

    // MARK: - Public

    public func configure(with color: UIColor) {
        backgroundColor = color
    }

    // MARK: - Private

    private func setupView() {
        layer.cornerRadius = frame.height / 2
        clipsToBounds = true
    }
}
