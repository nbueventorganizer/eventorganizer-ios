//
//  RegisterResponseModel.swift
//  EventOrganizer
//
//  Created by Alexander Karaatanasov on 13.01.19.
//  Copyright © 2019 Alexander Karaatanassov. All rights reserved.
//

import Foundation
import ObjectMapper

class RegisterResponseModel: ImmutableMappable {
    
    var message: String
    
    required init(map: Map) throws {
        message = try map.value("message")
    }
    
    func mapping(map: Map) {
        message <- map["message"]
    }
    
    init(message: String) {
        self.message = message
    }
}
