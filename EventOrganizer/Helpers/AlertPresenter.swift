//
//  AlertPresenter.swift
//  EventOrganizer
//
//  Created by Alexander Karaatanasov on 1.12.18.
//  Copyright © 2018 Alexander Karaatanassov. All rights reserved.
//

import Foundation
import UIKit

class AlertPresenter {
    
    private struct Alert {
        static let errorMessageSeparator = "\n"
    }
    
    static func showInformationAlert(from viewController: UIViewController,
                                     title: String,
                                     message: String,
                                     okAction: (target: NSObjectProtocol?, actionSelector: Selector)? = nil,
                                     completion: (() -> Void)? = nil) {
        let okAlertAction = CustomAlertAction(title: "OK", action: okAction)
        let alertController = CustomAlertViewController(title: title, message: message, okButton: okAlertAction, cancelButton: nil)
        
        viewController.present(alertController, animated: true, completion: completion)
    }

}

