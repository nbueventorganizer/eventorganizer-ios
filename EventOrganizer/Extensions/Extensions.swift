//
//  Extensions.swift
//  EventOrganizer
//
//  Created by Alexander Karaatanasov on 28.01.19.
//  Copyright © 2019 Alexander Karaatanassov. All rights reserved.
//

import UIKit

// MARK: tableview

extension UITableView {
    func getReusableCellWithIdentifier<T: UITableViewCell>(indexPath: IndexPath) -> T {
        guard let cell = self.dequeueReusableCell(withIdentifier: T.cellIdentifier, for: indexPath as IndexPath) as? T else {
            fatalError("Couldn't instantiate view controller with identifier \(T.cellIdentifier) ")
        }
        
        return cell
    }
}

// MARK: UITableViewCell

protocol TableViewCellIdentifiable {
    static var cellIdentifier: String { get }
}

extension TableViewCellIdentifiable where Self: UITableViewCell {
    static var cellIdentifier: String {
        return String(describing: self)
    }
}

extension UITableViewCell: TableViewCellIdentifiable {}

// MARK: storyboard

extension UIStoryboard {
    
    enum Storyboard: String {
        case Home
    }
    
    convenience init(storyboard: Storyboard, bundle: Bundle? = nil) {
        self.init(name: storyboard.rawValue, bundle: bundle)
    }
    
    class func storyboard(storyboard: Storyboard, bundle: Bundle? = nil) -> UIStoryboard {
        return UIStoryboard(name: storyboard.rawValue, bundle: bundle)
    }
    
    func instantiateViewController<T: UIViewController>() -> T {
        guard let viewController = self.instantiateViewController(withIdentifier: T.storyboardIdentifier) as? T else {
            fatalError("Couldn't instantiate view controller with identifier \(T.storyboardIdentifier) ")
        }
        
        return viewController
    }
}

extension UIViewController: StoryboardIdentifiable {}

// MARK: identifiable

protocol StoryboardIdentifiable {
    static var storyboardIdentifier: String { get }
}

extension StoryboardIdentifiable where Self: UIViewController {
    static var storyboardIdentifier: String {
        return String(describing: self)
    }
}

extension UIView {
    
    private func createAnimationFromKey(key: String, duration: Double, from: CGFloat, to: CGFloat, delay: Double = 0, remove: Bool = true) -> CABasicAnimation {
        let animation = CABasicAnimation(keyPath: key)
        animation.duration = duration
        animation.toValue = to
        animation.fromValue = from
        animation.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.easeOut)
        animation.beginTime = CACurrentMediaTime() + delay
        if remove == false {
            animation.isRemovedOnCompletion = remove
            animation.fillMode = CAMediaTimingFillMode.forwards
        }
        return animation
    }
    
    func rotateDuration(duration: Double, from: CGFloat, to: CGFloat, delay: Double = 0, remove: Bool = true) {
        let animation = createAnimationFromKey(key: "transform.rotation.z",
                                               duration: duration,
                                               from: from,
                                               to: to,
                                               delay: delay,
                                               remove: remove)
        layer.add(animation, forKey: nil)
    }
    
    func scaleDuration(duration: Double, from: CGFloat, to: CGFloat, delay: Double = 0, remove: Bool = true) {
        let animation = createAnimationFromKey(key: "transform.scale",
                                               duration: duration,
                                               from: from,
                                               to: to,
                                               delay: delay,
                                               remove: remove)
        
        layer.add(animation, forKey: nil)
    }
    
    func opacityDuration(duration: Double, from: CGFloat, to: CGFloat, delay: Double = 0, remove: Bool = true) {
        let animation = createAnimationFromKey(key: "opacity",
                                               duration: duration,
                                               from: from,
                                               to: to,
                                               delay: delay,
                                               remove: remove)
        
        layer.add(animation, forKey: nil)
    }
}

extension UIView {
    
    func makeScreenShotFromFrame(frame: CGRect) -> UIImage? {
        UIGraphicsBeginImageContextWithOptions(frame.size, false, 0.0)
        
        let context = UIGraphicsGetCurrentContext()
        context!.translateBy(x: frame.origin.x * -1, y: frame.origin.y * -1)
        
        guard let currentContext = UIGraphicsGetCurrentContext() else {
            return nil
        }
        
        layer.render(in: currentContext)
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return image
    }
}

extension UIImageView {
    
    func blurViewValue(value: CGFloat) {
        guard let image = self.image,
            let blurfilter = CIFilter(name: "CIGaussianBlur"),
            let imageToBlur = CIImage(image: image)
            else {
                return
        }
        
        blurfilter.setValue(value, forKey: kCIInputRadiusKey)
        blurfilter.setValue(imageToBlur, forKey: "inputImage")
        let resultImage = blurfilter.value(forKey: "outputImage") as! CIImage
        var blurredImage = UIImage(ciImage: resultImage)
        let cropped: CIImage = resultImage.cropped(to: CGRect(x: 0, y: 0, width: imageToBlur.extent.size.width, height: imageToBlur.extent.size.height))
        blurredImage = UIImage(ciImage: cropped)
        self.image = blurredImage
    }
}
