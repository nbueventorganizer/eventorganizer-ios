//
//  LoginModel.swift
//  EventOrganizer
//
//  Created by Alexander Karaatanasov on 13.01.19.
//  Copyright © 2019 Alexander Karaatanassov. All rights reserved.
//

import Foundation
import ObjectMapper

class LoginModel: ImmutableMappable {
    
    var email: String
    var password: String
    
    required init(map: Map) throws {
        email = try map.value("email")
        password = try map.value("password")
    }
    
    func mapping(map: Map) {
        email <- map["email"]
        password <- map["password"]
    }
    
    init(email: String, password: String) {
        self.email = email
        self.password = password
    }
}
