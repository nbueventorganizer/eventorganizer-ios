//
//  GradientButton.swift
//  EventOrganizer
//
//  Created by Alexander Karaatanasov on 30.11.18.
//  Copyright © 2018 Alexander Karaatanassov. All rights reserved.
//

import Foundation
import UIKit

@IBDesignable
public class GradientButton: RoundedButton {

    private struct Layout {
        static let disabledStateAlpha: CGFloat = 0.3
        static let enabledStateAlpha: CGFloat = 1
        static let defaultGradientDirection = (start: CGPoint(x: 0, y: 0.5), end: CGPoint(x: 1, y: 0.5))
    }

    @IBInspectable public var leftGradientColor: UIColor?
    @IBInspectable public var rightGradientColor: UIColor? { didSet { setupButton() } }
    @IBInspectable public var startPoint: CGPoint = Layout.defaultGradientDirection.start
    @IBInspectable public var endPoint: CGPoint = Layout.defaultGradientDirection.end

    override public init(frame: CGRect) {
        super.init(frame: frame)
        setupButton()
    }

    required public init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setupButton()
    }

    override public var isEnabled: Bool {
        didSet {
            changeButton()
        }
    }

    // MARK: - Private

    private func setupButton(topGradientColor: UIColor, bottomGradientColor: UIColor) {
        guard let leftColor = leftGradientColor, let rightColor = rightGradientColor else { return }
        let gradientColors = [leftColor, rightColor]
        let gradientView = GradientView(frame: frame,
                                        colors: gradientColors,
                                        startPoint: startPoint,
                                        endPoint: endPoint)
        gradientView.layer.cornerRadius = bounds.height/2

        insertSubview(gradientView, at: 0)

        changeButton()
    }
    
    private func setupButton() {
        guard let leftColor = leftGradientColor, let rightColor = rightGradientColor else { return }
        let gradientColors = [leftColor, rightColor]
        let gradientView = GradientView(frame: frame,
                                        colors: gradientColors,
                                        startPoint: startPoint,
                                        endPoint: endPoint)
        gradientView.layer.cornerRadius = bounds.height/2
        
        insertSubview(gradientView, at: 0)
        
        changeButton()
    }

    private func changeButton() {
        alpha = isEnabled == false ? Layout.disabledStateAlpha : Layout.enabledStateAlpha
    }
}
