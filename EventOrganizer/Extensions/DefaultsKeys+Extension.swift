//
//  DefaultsKeys+Extension.swift
//  EventOrganizer
//
//  Created by Alexander Karaatanasov on 13.01.19.
//  Copyright © 2019 Alexander Karaatanassov. All rights reserved.
//
import SwiftyUserDefaults

extension DefaultsKeys {
    static let token = DefaultsKey<String>("token")
    static let email = DefaultsKey<String>("email")
    static let userId = DefaultsKey<String>("userId")
    static let isOrganizer = DefaultsKey<String>("isOrganizer")
}
