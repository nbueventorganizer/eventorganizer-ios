//
//  HomeTableViewController.swift
//  EventOrganizer
//
//  Created by Alexander Karaatanasov on 1.12.18.
//  Copyright © 2018 Alexander Karaatanassov. All rights reserved.
//

import UIKit
import PreviewTransition

public class HomeTableViewController: PTTableViewController {
    
    fileprivate let items = [("1", "River cruise"), ("2", "North Island"), ("3", "Mountain trail"), ("4", "Southern Coast"), ("5", "Fishing place")]
    
    public override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationItem.title = "Home"
    }
}

// MARK: UITableViewDelegate

extension HomeTableViewController {
    
    public override func tableView(_: UITableView, numberOfRowsInSection _: Int) -> Int {
        return 100
    }
    
    public override func tableView(_: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        guard let cell = cell as? ParallaxCell else { return }
        
        let index = indexPath.row % items.count
        let imageName = items[index].0
        let title = items[index].1
        
        if let image = UIImage(named: imageName) {
            cell.setImage(image, title: title)
        }
    }
    
    public override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: ParallaxCell = tableView.getReusableCellWithIdentifier(indexPath: indexPath)
        return cell
    }
    
    public override func tableView(_: UITableView, didSelectRowAt _: IndexPath) {
        let storyboard = UIStoryboard.storyboard(storyboard: .Home)
        let detaleViewController: GalleryDetailViewController = storyboard.instantiateViewController()
        navigationItem.title = ""
        pushViewController(detaleViewController)
    }
}
