//
//  EventsTableViewController.swift
//  EventOrganizer
//
//  Created by Alexander Karaatanasov on 13.01.19.
//  Copyright © 2019 Alexander Karaatanassov. All rights reserved.
//

import UIKit
import FoldingCell
import SVProgressHUD

enum EventsType {
    case allEvents
    case organizerEvents
    case clientEvents
}

class EventsTableViewController: UITableViewController {
    
    private enum Constants {
        static let closeCellHeight: CGFloat = 179
        static let openCellHeight: CGFloat = 488
    }
    
    // MARK: - Vars
    
    private(set) var eventsType: EventsType = .allEvents
    
    private var cellHeights: [CGFloat] = []
    
    private var events: [EventModel] = []
    
    private var users: [UserModel] = []
    
    private var organizers: [OrganizerModel] = []
    
    private var types: [TypeModel] = []
    
    private var statuses: [String] = ["Pending", "Declined", "Accepted", "Confirmed"]
    
    private var selectedEvent: EventModel?
    
    // MARK: - Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        switch eventsType {
        case .allEvents:
            loadData()
        case .organizerEvents:
            loadOrganizerData()
            removeRightBarButton()
        case .clientEvents:
            loadClientData()
            removeRightBarButton()
        }
    }
    
    // MARK: - Navigation
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        switch segue.identifier {
        case "showCreateEvent":
            if let createEventController = segue.destination as? CreateEventTableViewController {
                createEventController.configure(with: eventsType, types: types, organizers: organizers)
                if let event = selectedEvent, eventsType == .organizerEvents {
                    createEventController.initializeCreateEvent(with: event)
                }
            }
        default:
            break
        }
    }
    
    // MARK: - Private
    
    private func loadData(manually userRefreshed: Bool = false) {
        if !userRefreshed { SVProgressHUD.show() }
        loadEvents { [weak self] events in
            self?.events = events
            self?.loadTypes { [weak self] types in
                self?.types = types
                self?.loadUsers { [weak self] users in
                    self?.users = users
                    self?.loadOrganizers { [weak self] organizers in
                        self?.organizers = organizers
                        if userRefreshed {
                            self?.tableView.refreshControl?.endRefreshing()
                            self?.setup()
                            self?.tableView.reloadData()
                        } else {
                            SVProgressHUD.dismiss();
                            self?.loadUI()
                        }
                    }
                }
            }
        }
    }
    
    private func loadOrganizerData(manually userRefreshed: Bool = false) {
        if !userRefreshed { SVProgressHUD.show() }
        loadOrganizerEvents { [weak self] events in
            self?.events = events
            self?.loadTypes { [weak self] types in
                self?.types = types
                self?.loadUsers { [weak self] users in
                    self?.users = users
                    self?.loadOrganizers { [weak self] organizers in
                        self?.organizers = organizers
                        if userRefreshed {
                            self?.tableView.refreshControl?.endRefreshing()
                            self?.setup()
                            self?.tableView.reloadData()
                        } else {
                            SVProgressHUD.dismiss();
                            self?.loadUI()
                        }
                    }
                }
            }
        }
    }
    
    private func loadClientData(manually userRefreshed: Bool = false) {
        if !userRefreshed { SVProgressHUD.show() }
        loadClientEvents { [weak self] events in
            self?.events = events
            self?.loadTypes { [weak self] types in
                self?.types = types
                self?.loadUsers { [weak self] users in
                    self?.users = users
                    self?.loadOrganizers { [weak self] organizers in
                        self?.organizers = organizers
                        if userRefreshed {
                            self?.tableView.refreshControl?.endRefreshing()
                            self?.setup()
                            self?.tableView.reloadData()
                        } else {
                            SVProgressHUD.dismiss();
                            self?.loadUI()
                        }
                    }
                }
            }
        }
    }
    
    private func loadUI() {
        switch eventsType {
        case .allEvents:
            setup()
        case .organizerEvents:
            setup()
            title = "Pending Events"
        case .clientEvents:
            setup()
            title = "Accepted Events"
        }
        
        tableView.reloadData()
        
        print("---------------- Users ----------------")
        for user in users { print("id: \(user.id) | name: \(user.name) | email: \(user.email)") }
        print("---------------------------------------")
        print("-------------- Organizers -------------")
        for organizer in organizers { print("id: \(organizer.id) | name: \(organizer.name)") }
        print("---------------------------------------")
        print("---------------- Types ----------------")
        for type in types { print("id: \(type.id) | title: \(type.title)") }
        print("---------------------------------------")
        print("Events type: \(eventsType)")
    }
    
    private func setup() {
        cellHeights = Array(repeating: Constants.closeCellHeight, count: events.count)
        tableView.estimatedRowHeight = Constants.closeCellHeight
        tableView.rowHeight = UITableView.automaticDimension
        tableView.backgroundColor = UIColor(patternImage: #imageLiteral(resourceName: "background")) // MARK: - Change Background Image from here
        if #available(iOS 10.0, *) {
            tableView.refreshControl = UIRefreshControl()
            tableView.refreshControl?.addTarget(self, action: #selector(refreshHandler), for: .valueChanged)
        }
    }
    
    private func removeRightBarButton() {
        self.navigationItem.setRightBarButton(nil, animated: false)
    }
    
    @objc private func refreshHandler() {
        switch eventsType {
        case .allEvents:
            loadData(manually: true)
        case .organizerEvents:
            loadOrganizerData(manually: true)
        case .clientEvents:
            loadClientData(manually: true)
        }
    }

    // MARK: - Requests
    
    // all events
    private func loadEvents(completionHandler: @escaping (([EventModel]) -> Void)) {
        PublicManagerAPI.sharedInstance.getEvents { [weak self] (events, error) in
            if let response = events {
                completionHandler(response.events)
            } else if let error = error, let controller = self {
                AlertPresenter.showInformationAlert(from: controller, title: "Sorry", message: error.localizedDescription)
            }
        }
    }
    // all organizer events
    private func loadOrganizerEvents(completionHandler: @escaping (([EventModel]) -> Void)) {
        PrivateManagerAPI.sharedInstance.getOrganizerEvents { [weak self] (events, error) in
            if let response = events {
                completionHandler(response.events)
            } else if let error = error, let controller = self {
                AlertPresenter.showInformationAlert(from: controller, title: "Sorry", message: error.localizedDescription)
            }
        }
    }
    // all client events
    private func loadClientEvents(completionHandler: @escaping (([EventModel]) -> Void)) {
        PrivateManagerAPI.sharedInstance.getClientEvents { [weak self] (events, error) in
            if let response = events {
                completionHandler(response.events)
            } else if let error = error, let controller = self {
                AlertPresenter.showInformationAlert(from: controller, title: "Sorry", message: error.localizedDescription)
            }
        }
    }
    
    private func loadTypes(completionHandler: @escaping (([TypeModel]) -> Void)) {
        PublicManagerAPI.sharedInstance.getTypes { [weak self] (types, error) in
            if let response = types {
                completionHandler(response.types)
            } else if let error = error, let controller = self {
                AlertPresenter.showInformationAlert(from: controller, title: "Sorry", message: error.localizedDescription)
            }
        }
    }
    
    private func loadUsers(completionHandler: @escaping (([UserModel]) -> Void)) {
        PublicManagerAPI.sharedInstance.getUsers { [weak self] (users, error) in
            if let response = users {
                completionHandler(response.users)
            } else if let error = error, let controller = self {
                AlertPresenter.showInformationAlert(from: controller, title: "Sorry", message: error.localizedDescription)
            }
        }
    }
    
    private func loadOrganizers(completionHandler: @escaping (([OrganizerModel]) -> Void)) {
        PublicManagerAPI.sharedInstance.getOrganizers { [weak self] (organizers, error) in
            if let response = organizers {
                completionHandler(response.organizers)
            } else if let error = error, let controller = self {
                AlertPresenter.showInformationAlert(from: controller, title: "Sorry", message: error.localizedDescription)
            }
        }
    }
    
    // MARK: - Public
    
    func setScreenType(for type: EventsType) {
        eventsType = type
    }
    
    func reloadData() {
        loadData()
    }
    
    func getTypes() -> [TypeModel] { return types }
    
    func getUsers() -> [UserModel] { return users }
    
    func getOrganizers() -> [OrganizerModel] { return organizers }
    
    func getStatuses() -> [String] { return statuses }
    
    func showCreateEvent() {
        performSegue(withIdentifier: "showCreateEvent", sender: self)
    }
    
    // MARK: - IBActions
    
    @IBAction func createEvent(_ sender: Any) {
        showCreateEvent()
    }
    
}

// MARK: - Table View Methods

extension EventsTableViewController {
    
    override func tableView(_: UITableView, numberOfRowsInSection _: Int) -> Int {
        return events.count
    }
    
    override func tableView(_: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        guard case let cell as EventCell = cell else {
            return
        }
        
        if cellHeights[indexPath.row] == Constants.closeCellHeight {
            cell.unfold(false, animated: false, completion: nil)
        } else {
            cell.unfold(true, animated: false, completion: nil)
        }
        
        cell.backgroundColor = .clear
        cell.number = indexPath.row + 1
        cell.configure(from: self, with: events[indexPath.row], buttons: eventsType == .allEvents ? true : false)
        selectedEvent = events[indexPath.row]
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "FoldingCell", for: indexPath) as! FoldingCell
        let durations: [TimeInterval] = [0.26, 0.2, 0.2]
        cell.durationsForExpandedState = durations
        cell.durationsForCollapsedState = durations
        return cell
    }
    
    override func tableView(_: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return cellHeights[indexPath.row]
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let cell = tableView.cellForRow(at: indexPath) as! FoldingCell
        if cell.isAnimating() {
            return
        }
        
        var duration = 0.0
        let cellIsCollapsed = cellHeights[indexPath.row] == Constants.closeCellHeight
        if cellIsCollapsed {
            cellHeights[indexPath.row] = Constants.openCellHeight
            cell.unfold(true, animated: true, completion: nil)
            duration = 0.5
        } else {
            cellHeights[indexPath.row] = Constants.closeCellHeight
            cell.unfold(false, animated: true, completion: nil)
            duration = 0.8
        }
        
        UIView.animate(withDuration: duration, delay: 0, options: .curveEaseOut, animations: { () -> Void in
            tableView.beginUpdates()
            tableView.endUpdates()
        }, completion: nil)
    }
}
