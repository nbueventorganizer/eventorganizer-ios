//
//  CustomNavigationController.swift
//  EventOrganizer
//
//  Created by Alexander Karaatanasov on 30.11.18.
//  Copyright © 2018 Alexander Karaatanassov. All rights reserved.
//

import Foundation
import UIKit

public class CustomNavigationController: UINavigationController {
    
    override public func viewDidLoad() {
        super.viewDidLoad()
        
        delegate = self
        configureNavigationBar()
    }
    
    // MARK: - Private
    
    private func configureNavigationBar() {
        navigationBar.isTranslucent = true
        navigationBar.setBackgroundImage(UIImage(), for: .default)
        navigationBar.shadowImage = UIImage()
        navigationBar.backgroundColor = .clear
    }
    
}

// MARK: - UINavigationControllerDelegate

extension CustomNavigationController: UINavigationControllerDelegate {
    public func navigationController(_ navigationController: UINavigationController, willShow viewController: UIViewController, animated: Bool) {
        let barButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        viewController.navigationItem.backBarButtonItem = barButtonItem
    }
}
