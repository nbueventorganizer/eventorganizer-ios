//
//  EventsModel.swift
//  EventOrganizer
//
//  Created by Alexander Karaatanasov on 12.01.19.
//  Copyright © 2019 Alexander Karaatanassov. All rights reserved.
//

import Foundation
import ObjectMapper

class EventsModel: ImmutableMappable {
    
    var events: [EventModel]
    
    required init(map: Map) throws {
        events = (try? map.value("events")) ?? []
    }
    
    func mapping(map: Map) {
        events <- map["events"]
    }
    
    init(events: [EventModel]) {
        self.events = events
    }
}
