//
//  AppConstant.swift
//  EventOrganizer
//
//  Created by Alexander Karaatanasov on 30.11.18.
//  Copyright © 2018 Alexander Karaatanassov. All rights reserved.
//

import Foundation
import UIKit

struct Font {
    static let heading1 = UIFont(name: "Avenir-Heavy", size: 33.0)!
    static let heading2 = UIFont(name: "Avenir-Heavy", size: 24.0)!
    
    static let body1 = UIFont(name: "Avenir-Light", size: 16.0)!
}

struct Animation {
    static let squeezeDuration: TimeInterval = 0.3
    static let squeezeGradientAnimationName = "loading_animation_gradient"
    static let squeezeAnimationName = "loading_animation"
}

struct Alert {
    static let cornerRadius: CGFloat = 7
    static let backgroundAlpha: CGFloat = 0.4
    static let backgroundDimDuration: TimeInterval = 0.4
}

struct Color {
    static let primaryButtonLeft = R.color.primaryButtonFirstColor()!
    static let primaryButtonRight = R.color.primaryButtonSecondColor()!
}

struct GlobalColor {
    static let shadow: UIColor = R.color.shadowColor()!
    static let viewBorder = R.color.viewBorderColor()!
    static let background = UIColor.clear
}

struct GlobalLayout {
    static let shadowOffset = CGSize(width: 0, height: 5)
    static let shadowOpacity: Float = 1
    static let shadowRadius: CGFloat = 8
    static let viewBorderWidth: CGFloat = 1
    static let viewCornerRadius: CGFloat = 10
    static let viewMasksToBounds = true
}

struct Units {
    static let kilometerInMeters = 1000
    static let defaultTimeFormat = "HH:mm"
}

struct Key {
    static let appLaunched = "appLaunched"
    static let sessionState = "sessionState"
    static let heartRate = "heartRate"
}

struct Permissions {
    static let read = ["public_profile", "email"]
}

struct API {
    static let baseURL = "http://dev.weband.bg/eventorganizer-api/public"
}
