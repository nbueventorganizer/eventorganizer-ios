//
//  UITextField+Extension.swift
//  EventOrganizer
//
//  Created by Alexander Karaatanasov on 30.11.18.
//  Copyright © 2018 Alexander Karaatanassov. All rights reserved.
//

import Foundation
import UIKit

private var associationKeyNextField: UInt8 = 0

extension UITextField {
    
    @IBOutlet public var nextField: UITextField? {
        get {
            return objc_getAssociatedObject(self, &associationKeyNextField) as? UITextField
        }
        set(newField) {
            objc_setAssociatedObject(self, &associationKeyNextField, newField, .OBJC_ASSOCIATION_RETAIN)
        }
    }
    
}
