//
//  OrganizersModel.swift
//  EventOrganizer
//
//  Created by Alexander Karaatanasov on 13.01.19.
//  Copyright © 2019 Alexander Karaatanassov. All rights reserved.
//

import Foundation
import ObjectMapper

class OrganizersModel: ImmutableMappable {
    
    var organizers: [OrganizerModel]
    
    required init(map: Map) throws {
        organizers = (try? map.value("organizers")) ?? []
    }
    
    func mapping(map: Map) {
        organizers <- map["organizers"]
    }
    
    init(organizers: [OrganizerModel]) {
        self.organizers = organizers
    }
}
