//
//  KeychainAccess.swift
//  EventOrganizer
//
//  Created by Alexander Karaatanasov on 1.12.18.
//  Copyright © 2018 Alexander Karaatanassov. All rights reserved.
//

import Foundation
import KeychainAccess

class KeychainAccess {
    let keychain = Keychain(service: KeychainKey.service)
    
    static let sharedInstance = KeychainAccess()
    
    private init() {}
}
