//
//  JVFloatTextField+Extension.swift
//  EventOrganizer
//
//  Created by Alexander Karaatanasov on 30.11.18.
//  Copyright © 2018 Alexander Karaatanassov. All rights reserved.
//

import Foundation
import JVFloatLabeledTextField

@IBDesignable
public class FloatingTextField: JVFloatLabeledTextField {

    private struct ColorConstant {
        static let textFieldBorder = R.color.textFieldBorder()!
        static let textFieldPlaceholder = R.color.textFieldPlaceholderColor()!
    }

    private struct Layout {
        static let cornerRadius: CGFloat = 10
        static let borderWidth: CGFloat = 1
        static let leftInset: CGFloat = 16
    }

    @IBInspectable public var cornerRadius: CGFloat = Layout.cornerRadius {
        didSet {
            layer.cornerRadius = cornerRadius
        }
    }

    @IBInspectable public var borderWidth: CGFloat = Layout.borderWidth {
        didSet {
            layer.borderWidth = borderWidth
        }
    }

    @IBInspectable public var borderColor: UIColor = ColorConstant.textFieldBorder {
        didSet {
            layer.borderColor = borderColor.cgColor
        }
    }

    @IBInspectable public var leftInset: CGFloat = Layout.leftInset

    override public init(frame: CGRect) {
        super.init(frame: frame)
        setupTextField()
    }

    required public init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setupTextField()
    }

    // MARK: Private

    private func setupColors() {
        placeholderColor = ColorConstant.textFieldPlaceholder
        floatingLabelTextColor = ColorConstant.textFieldPlaceholder
        floatingLabelActiveTextColor = ColorConstant.textFieldPlaceholder
    }

    private func setupTextField() {
        layer.cornerRadius = cornerRadius
        layer.borderWidth = borderWidth
        layer.borderColor = borderColor.cgColor

        setupColors()

        leftView = UIView(frame: CGRect(x: 0, y: 0, width: leftInset, height: frame.height))
        leftViewMode = .always
    }

}
