//
//  CreateEventModel.swift
//  EventOrganizer
//
//  Created by Alexander Karaatanasov on 13.01.19.
//  Copyright © 2019 Alexander Karaatanassov. All rights reserved.
//

import Foundation
import ObjectMapper

class CreateEventModel: ImmutableMappable, Equatable {
    
    let title: String?
    let organizerId: Int?
    let typeId: Int?
    let description: String?
    let date: String?
    let involved: String?
    let exactDate: String?
    let location: String?
    let feedingPrice: Double?
    let accommodationPrice: Double?
    let pricePerPerson: Double?
    
    required init(map: Map) throws {
        title = try map.value("title")
        organizerId = try map.value("organizer")
        typeId = try map.value("type")
        description = try map.value("description")
        date = try map.value("date")
        involved = try map.value("involved")
        exactDate = try map.value("exact_date")
        location = try map.value("location")
        feedingPrice = try map.value("feeding_price")
        accommodationPrice = try map.value("accommodation_price")
        pricePerPerson = try map.value("price_per_person")
    }
    
    func mapping(map: Map) {
        title >>> map["title"]
        organizerId >>> map["organizer"]
        typeId >>> map["type"]
        description >>> map["description"]
        date >>> map["date"]
        involved >>> map["involved"]
        exactDate >>> map["exact_date"]
        location >>> map["location"]
        feedingPrice >>> map["feeding_price"]
        accommodationPrice >>> map["accommodation_price"]
        pricePerPerson >>> map["price_per_persion"]
    }
    
    init(title: String? = nil, organizerId: Int? = nil, typeId: Int? = nil, description: String? = nil, date: String? = nil, involved: String? = nil, exactDate: String? = nil, location: String? = nil, feedingPrice: Double? = nil, accommodationPrice: Double? = nil, pricePerPerson: Double? = nil) {
        self.title = title
        self.organizerId = organizerId
        self.typeId = typeId
        self.description = description
        self.date = date
        self.involved = involved
        self.exactDate = exactDate
        self.location = location
        self.feedingPrice = feedingPrice
        self.accommodationPrice = accommodationPrice
        self.pricePerPerson = pricePerPerson
    }
    
    static func == (lhs: CreateEventModel, rhs: CreateEventModel) -> Bool {
        return lhs.title == rhs.title
    }
}
