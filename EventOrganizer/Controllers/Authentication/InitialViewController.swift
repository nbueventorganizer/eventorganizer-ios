//
//  InitialViewController.swift
//  EventOrganizer
//
//  Created by Alexander Karaatanasov on 1.12.18.
//  Copyright © 2018 Alexander Karaatanassov. All rights reserved.
//

import UIKit

class InitialViewController: UIViewController {
    
    // MARK: - Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        UserAuthentication.isOrganizerUnSet()
    }

    // MARK: - IBAction
    
    @IBAction func takeALook(_ sender: UIButton) {
        ControllerManager.switchRootController(to: .home)
    }
    
}
