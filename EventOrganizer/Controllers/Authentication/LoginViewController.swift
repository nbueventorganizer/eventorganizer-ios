//
//  LoginViewController.swift
//  EventOrganizer
//
//  Created by Alexander Karaatanassov on 29.11.18.
//  Copyright © 2018 Alexander Karaatanassov. All rights reserved.
//

import UIKit
import SVProgressHUD
import SwiftyUserDefaults

class LoginViewController: InputViewController {
    
    // MARK: - IBOutlet
    
    @IBOutlet weak var emailTextField: FloatingTextField!
    @IBOutlet weak var passwordTextField: FloatingTextField!
    @IBOutlet weak var loginButton: GradientButton!
    
    // MARK: - Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
        navigationController?.navigationBar.prefersLargeTitles = true
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        view.endEditing(true)
    }
    
    deinit {
        navigationController?.navigationBar.prefersLargeTitles = false
    }
    
    // MARK: - Private
    
    private func setupView() {
        print("Login")
    }
    
    private func loginUser(with email: String, password: String) {
        SVProgressHUD.show()
        let loginModel = LoginModel(email: email, password: password)
        PublicManagerAPI.sharedInstance.login(with: loginModel, completionHandler: { (response, error) in
            SVProgressHUD.dismiss()
            if let tokenModel = response {
                UserAuthentication.login(with: tokenModel)
                ControllerManager.switchRootController(to: .home)
            } else if let error = error {
                AlertPresenter.showInformationAlert(from: self, title: "Oops", message: error.error)
            }
        })
    }
    
    // MARK: - IBActions
    
    @IBAction func textFieldChanged(_ sender: FloatingTextField) {
        print("--------------------------------")
        print("Email: \(emailTextField.text!)")
        print("Password: \(passwordTextField.text!)")
        print("--------------------------------")
    }
    
    @IBAction func login(_ sender: GradientButton) {
        view.endEditing(true)
        if let email = emailTextField.text, let password = passwordTextField.text {
            loginUser(with: email, password: password)
        }
    }
    
    @IBAction func tap(_ sender: UITapGestureRecognizer) {
        view.endEditing(true)
    }
    
}

