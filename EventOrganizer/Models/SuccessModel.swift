//
//  SuccessModel.swift
//  EventOrganizer
//
//  Created by Alexander Karaatanasov on 13.01.19.
//  Copyright © 2019 Alexander Karaatanassov. All rights reserved.
//

import Foundation
import ObjectMapper

class SuccessModel: ImmutableMappable, Error {
    var success: Bool
    
    required init(map: Map) throws {
        success = try map.value("success")
    }
    
    func mapping(map: Map) {
        success <- map["success"]
//        success >>> map["success"] // uncomment if doesn't work
    }
    
    init(success: Bool) {
        self.success = success
    }
}
