//
//  TypeModel.swift
//  EventOrganizer
//
//  Created by Alexander Karaatanasov on 13.01.19.
//  Copyright © 2019 Alexander Karaatanassov. All rights reserved.
//

import Foundation
import ObjectMapper
import Sheeeeeeeeet

class TypeModel: ImmutableMappable {
    // - TODO: Add all properties
    var id: Int
    var alias: String
    var title: String
    
    required init(map: Map) throws {
        id = try map.value("id")
        alias = try map.value("alias")
        title = try map.value("title")
    }
    
    func mapping(map: Map) {
        id <- map["id"]
        alias <- map["alias"]
        title <- map["title"]
    }
    
    init(id: Int, alias: String, title: String) {
        self.id = id
        self.alias = alias
        self.title = title
    }
}

extension TypeModel {
    func item() -> ActionSheetItem {
        return ActionSheetItem(title: title)
    }
}
