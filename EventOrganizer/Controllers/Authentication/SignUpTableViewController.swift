//
//  SignUpTableViewController.swift
//  EventOrganizer
//
//  Created by Alexander Karaatanasov on 30.11.18.
//  Copyright © 2018 Alexander Karaatanassov. All rights reserved.
//

import UIKit
import Sheeeeeeeeet
import SVProgressHUD

class SignUpTableViewController: InputTableViewController {
    
    var roleOptions: [RoleOption] = [.client, .organizer]

    // MARK: - IBOutlets

    @IBOutlet weak var firstNameTextField: FloatingTextField!
    @IBOutlet weak var lastNameTextField: FloatingTextField!
    @IBOutlet weak var emailTextField: FloatingTextField!
    @IBOutlet weak var passwordTextField: FloatingTextField!
    @IBOutlet weak var confirmPasswordTextField: FloatingTextField!
    @IBOutlet weak var roleTextField: FloatingTextField!
    
    // MARK: - Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationController?.navigationBar.prefersLargeTitles = true
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        view.endEditing(true)
    }
    
    deinit {
        navigationController?.navigationBar.prefersLargeTitles = false
    }
    
    // MARK: - Private

    private func presentActionSheet(from view: UIView?) {
        let sheet = StandardActionSheet(options: roleOptions, action: setSelectedRole)
        sheet.presenter.events.didDismissWithBackgroundTap = { print("Background tap!") }
        sheet.present(in: self, from: view)
    }
    
    private func setSelectedRole(items: [ActionSheetItem]) {
        let items = items.filter { !($0 is ActionSheetButton) }
        guard let selectedRole = items.first?.title,
            items.count > 0 else { return }
        print("Selected: \(selectedRole)")
        roleTextField.text = selectedRole
    }
    
    // MARK: - Requests
    
    private func registerUser(with name: String, email: String, password: String, role: String) {
        SVProgressHUD.show()
        let registerModel = RegisterModel(name: name, email: email, password: password, role: role)
        PublicManagerAPI.sharedInstance.register(with: registerModel, completionHandler: { (response, error) in
            SVProgressHUD.dismiss()
            self.loginUser(with: email, password: password)
        })
    }
    
    private func loginUser(with email: String, password: String) {
        SVProgressHUD.show()
        let loginModel = LoginModel(email: email, password: password)
        PublicManagerAPI.sharedInstance.login(with: loginModel, completionHandler: { (response, error) in
            SVProgressHUD.dismiss()
            if let tokenModel = response {
                UserAuthentication.login(with: tokenModel)
                ControllerManager.switchRootController(to: .home)
            } // handle error
        })
    }
    
    // MARK: - IBActions
    
    @IBAction func editingChanged(_ sender: FloatingTextField) {
        print("--------------------------------")
        print("First name: \(firstNameTextField.text!)")
        print("Last name: \(lastNameTextField.text!)")
        print("Email: \(emailTextField.text!)")
        print("Password: \(passwordTextField.text!)")
        print("Confirm Password: \(confirmPasswordTextField.text!)")
        print("Role: \(roleTextField.text!)")
        print("--------------------------------")
    }
  
    @IBAction func signup(_ sender: GradientButton) {
        tableView.endEditing(true)
        
        if let firstName = firstNameTextField.text,
            let lastName = lastNameTextField.text,
            let email = emailTextField.text,
            let password = passwordTextField.text,
            let confirmPassword = confirmPasswordTextField.text,
            let role = roleTextField.text {
            
            guard password == confirmPassword else {
                AlertPresenter.showInformationAlert(from: self,
                                                    title: "Oops",
                                                    message: "Please make sure both passwords match")
                return
            }
            
            guard role == "Client" || role == "Organizer" else {
                AlertPresenter.showInformationAlert(from: self,
                                                    title: "Oops",
                                                    message: "Please enter your role")
                return
            }
            
            registerUser(with: "\(firstName) \(lastName)", email: email, password: password, role: role)
        }
    }
    
    @IBAction func tap(_ sender: UITapGestureRecognizer) {
        tableView.endEditing(true)
    }

}

extension SignUpTableViewController {
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.row == 5 { presentActionSheet(from: tableView) } // using this to present picker
    }
}
