//
//  ViewModifier.swift
//  EventOrganizer
//
//  Created by Alexander Karaatanasov on 1.12.18.
//  Copyright © 2018 Alexander Karaatanassov. All rights reserved.
//

import Foundation
import UIKit

class ViewModifier {
    
    static func addShadow(to view: UIView, with radius: CGFloat) {
        view.layer.backgroundColor = GlobalColor.background.cgColor
        view.layer.shadowColor = GlobalColor.shadow.cgColor
        view.layer.shadowOffset = GlobalLayout.shadowOffset
        view.layer.shadowOpacity = GlobalLayout.shadowOpacity
        view.layer.shadowRadius = radius
    }
    
    static func roundEdges(to view: UIView, with radius: CGFloat) {
        view.layer.borderColor = GlobalColor.viewBorder.cgColor
        view.layer.borderWidth = GlobalLayout.viewBorderWidth
        view.layer.cornerRadius = radius
        view.layer.masksToBounds = GlobalLayout.viewMasksToBounds
    }
    
}
