//
//  CustomAlertView.swift
//  CustomAlertView
//
//  Created by Daniel Luque Quintana on 16/3/17.
//  Copyright © 2017 dluque. All rights reserved.
//

import UIKit

public class CustomAlertViewController: UIViewController {
    
    @IBOutlet weak var alertView: UIView! {
        didSet {
            alertView.layer.cornerRadius = Alert.cornerRadius
            alertView.layer.masksToBounds = true
        }
    }
    
    @IBOutlet weak var actionButtonsStackView: UIStackView!
    
    @IBOutlet weak var titleLabel: UILabel! {
        didSet {
            titleLabel.text = titleText
        }
    }
    @IBOutlet weak var messageLabel: UILabel! {
        didSet {
            messageLabel.text = message
        }
    }
    
    @IBOutlet weak var okButton: GradientButton! {
        didSet {
            setupButton(okButton, descriptor: okButtonDescriptor)
        }
    }
    @IBOutlet weak var cancelButton: RoundedButton! {
        didSet {
            setupButton(cancelButton, descriptor: cancelButtonDescriptor)
        }
    }
    
    private let okButtonDescriptor: CustomAlertAction?
    private let cancelButtonDescriptor: CustomAlertAction?
    
    private let message: String?
    private let titleText: String?
    
    // MARK: - Lifecycle
    
    override public func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        dimBackground()
    }
    
    public override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
    }
    
    public init(title: String?,
                message: String? = nil,
                okButton: CustomAlertAction? = nil,
                cancelButton: CustomAlertAction? = nil) {
        self.titleText = title
        self.message = message
        
        self.okButtonDescriptor = okButton
        self.cancelButtonDescriptor = cancelButton
        
        super.init(nibName: R.nib.customAlertViewController.name, bundle: R.nib.customAlertViewController.bundle)
        
        self.setupTransition()
    }
    
    required public init?(coder aDecoder: NSCoder) {
        message = nil
        titleText = nil
        okButtonDescriptor = nil
        cancelButtonDescriptor = nil
        
        super.init(coder: aDecoder)
        
    }
    
    // MARK: - Private
    
    private func setupTransition() {
        providesPresentationContextTransitionStyle = true
        definesPresentationContext = true
        modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        modalTransitionStyle = UIModalTransitionStyle.crossDissolve
    }
    
    private func dimBackground() {
        UIView.animate(withDuration: Alert.backgroundDimDuration, animations: { [weak self] in
            self?.view.backgroundColor = UIColor.black.withAlphaComponent(Alert.backgroundAlpha)
        })
    }
    
    private func setupButton(_ button: UIButton, descriptor: CustomAlertAction?) {
        if let descriptor = descriptor {
            button.setTitle(descriptor.title, for: .normal)
        } else {
            button.isHidden = true
        }
    }
    
    @IBAction private func submit() {
        if let action = okButtonDescriptor?.action {
            _ = action.target?.perform(action.actionSelector)
        }
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction private func cancel() {
        if let action = cancelButtonDescriptor?.action {
            _ = action.target?.perform(action.actionSelector)
        }
        self.dismiss(animated: true, completion: nil)
    }
}
