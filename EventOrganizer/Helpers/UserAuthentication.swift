//
//  UserAuthentication.swift
//  EventOrganizer
//
//  Created by Alexander Karaatanasov on 1.12.18.
//  Copyright © 2018 Alexander Karaatanassov. All rights reserved.
//

import Foundation
import SwiftyUserDefaults

class UserAuthentication {
    
    static func isOrganizerUnSet() {
        Defaults[.isOrganizer] = ""
    }
    
    static func isOrganizerSet(flag: Bool) {
        Defaults[.isOrganizer] = "\(flag)"
    }
    
    static func login(with model: ResponseTokenModel) {
        Defaults[.token] = model.token
        Defaults[.email] = model.user.email
        Defaults[.userId] = "\(model.user.id)"
    }
    
    static func logout() {
        Defaults[.token] = ""
    }
    
    static func getToken() -> String {
        return Defaults[.token]
    }
    
    static func getEmail() -> String {
        return Defaults[.email]
    }
    
    static func getUserId() -> String {
        return Defaults[.userId]
    }
    
    static func userIsOrganizer() -> Bool {
        return Defaults[.isOrganizer] == "true"
    }
    
    static func isOrganizerNotSet() -> Bool {
        return Defaults[.isOrganizer] == ""
    }
    
    static func userIsLoggedIn() -> Bool {
        return Defaults[.token] != ""
    }
    
}
