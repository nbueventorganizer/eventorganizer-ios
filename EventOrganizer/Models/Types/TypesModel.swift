//
//  TypesModel.swift
//  EventOrganizer
//
//  Created by Alexander Karaatanasov on 13.01.19.
//  Copyright © 2019 Alexander Karaatanassov. All rights reserved.
//

import Foundation
import ObjectMapper

class TypesModel: ImmutableMappable {
    
    var types: [TypeModel]
    
    required init(map: Map) throws {
        types = (try? map.value("types")) ?? []
    }
    
    func mapping(map: Map) {
        types <- map["types"]
    }
    
    init(types: [TypeModel]) {
        self.types = types
    }
}
