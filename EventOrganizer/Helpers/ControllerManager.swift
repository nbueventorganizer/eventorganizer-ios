//
//  ControllerManager.swift
//  EventOrganizer
//
//  Created by Alexander Karaatanasov on 1.12.18.
//  Copyright © 2018 Alexander Karaatanassov. All rights reserved.
//

import Foundation
import UIKit

class ControllerManager {
    
    // MARK: - Vars
    
    enum RootControllers {
        case login
        case home
        
        var viewController: UIViewController {
            switch self {
            case .login:
                return R.storyboard.authentication.initialScreen()!
            case .home:
                return R.storyboard.home.initialScreen()!
            }
        }
        
    }
    
    static var top: UIViewController? {
        return topViewController()
    }
    
    static var root: UIViewController? {
        return UIApplication.shared.delegate?.window??.rootViewController
    }
    
    // MARK: - Public
    
    class func switchRootController(to controller: RootControllers, transitionType: CATransitionType = CATransitionType.fade, transitionSubType: String? = nil) {
        switchRootController(controller.viewController, transitionType: transitionType, transitionSubType: transitionSubType)
    }
    
    static func switchRootController(_ controller: UIViewController?, transitionType: CATransitionType = CATransitionType.fade, transitionSubType: String? = nil) {
        let window = UIApplication.shared.windows.first
        let transition = CATransition()
        transition.type = transitionType
        transition.subtype = transitionSubType.map { CATransitionSubtype(rawValue: $0) }
        
        if let rootController = controller {
            window?.setRootViewController(rootController, transition: transition)
        }
    }
    
    static func topViewController(from viewController: UIViewController? = root) -> UIViewController? {
        if let tabBarViewController = viewController as? UITabBarController {
            return topViewController(from: tabBarViewController.selectedViewController)
        } else if let navigationController = viewController as? UINavigationController {
            return topViewController(from: navigationController.visibleViewController)
        } else if let presentedViewController = viewController?.presentedViewController {
            return topViewController(from: presentedViewController)
        } else {
            return viewController
        }
    }
    
    static func openSettings() {
        guard let settingsUrl = URL(string: UIApplication.openSettingsURLString) else {
            return
        }
        if UIApplication.shared.canOpenURL(settingsUrl) {
            UIApplication.shared.open(settingsUrl)
        }
    }
}

