//
//  InputTableViewController.swift
//  EventOrganizer
//
//  Created by Alexander Karaatanasov on 30.11.18.
//  Copyright © 2018 Alexander Karaatanassov. All rights reserved.
//

import UIKit

class InputTableViewController: UITableViewController {}

// МАRК: - UITextFieldDelegate

extension InputTableViewController: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if let nextTextField = textField.nextField {
            nextTextField.becomeFirstResponder()
        } else {
            textField.resignFirstResponder()
        }
        return true
    }
}
