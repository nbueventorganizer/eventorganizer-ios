//
//  AccessTokenModel.swift
//  EventOrganizer
//
//  Created by Alexander Karaatanasov on 13.01.19.
//  Copyright © 2019 Alexander Karaatanassov. All rights reserved.
//

import Foundation
import ObjectMapper

class ResponseTokenModel: ImmutableMappable {
    
    let token: String
    let user: UserModel
    
    required init(map: Map) throws {
        token = try map.value("token")
        user = try map.value("user")
    }
    
    func mapping(map: Map) {
        token >>> map["token"]
        user >>> map["user"]
    }
    
    init(token: String, user: UserModel) {
        self.token = token
        self.user = user
    }
    
}
