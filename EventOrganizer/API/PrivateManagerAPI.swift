//
//  PrivateManagerAPI.swift
//  EventOrganizer
//
//  Created by Alexander Karaatanasov on 12.01.19.
//  Copyright © 2019 Alexander Karaatanassov. All rights reserved.
//

import Foundation
import Alamofire
import AlamofireObjectMapper
import SwiftyUserDefaults

class PrivateManagerAPI: BaseManagerAPI {
    
    override init() {
        super.init()
    }
    
    class var sharedInstance: PrivateManagerAPI {
        struct Static {
            static let instance = PrivateManagerAPI()
        }
        
        return Static.instance
    }
    
    // - MARK: Requests

    // 1. send event from client
    func sendCreateEvent(with event: CreateEventModel, completionHandler: @escaping ((SuccessModel?, ErrorModel?) -> Void)) {
        performObjectRequest(PrivateManagerRouter.sendCreateEvent(with: event), completionHandler: completionHandler)
    }
    
    // 2.1 accept event from organizator
    func acceptEvent(with event: CreateEventModel, and id: Int, completionHandler: @escaping (([ArrayModel]?, ErrorModel?) -> Void)) {
        performArrayRequest(PrivateManagerRouter.acceptEvent(with: event, id: id), completionHandler: completionHandler)
    }
    
    // 2.2 & 3.2 decline event from client AND organizator
    func declineEvent(with id: Int, completionHandler: @escaping (([ArrayModel]?, ErrorModel?) -> Void)) {
        performArrayRequest(PrivateManagerRouter.declineEvent(id: id), completionHandler: completionHandler)
    }
    
    // 3.1 confirm event from client
    func confirmEvent(with id: Int, completionHandler: @escaping (([ArrayModel]?, ErrorModel?) -> Void)) {
        performArrayRequest(PrivateManagerRouter.confirmEvent(id: id), completionHandler: completionHandler)
    }
    
    // get user specific events (profile screen)
    func getOrganizerEvents(completionHandler: @escaping ((EventsModel?, ErrorModel?) -> Void)) {
        performObjectRequest(PrivateManagerRouter.getOrganizerEvents, completionHandler: completionHandler)
    }
    
    func getClientEvents(completionHandler: @escaping ((EventsModel?, ErrorModel?) -> Void)) {
        performObjectRequest(PrivateManagerRouter.getClientEvents, completionHandler: completionHandler)
    }
    
    // - MARK: Router
    
    private enum PrivateManagerRouter: URLRequestConvertible {
        static let baseURLString = API.baseURL
        
        case sendCreateEvent(with: CreateEventModel)
        
        case acceptEvent(with: CreateEventModel, id: Int)
        case declineEvent(id: Int)
        case confirmEvent(id: Int)
        
        case getOrganizerEvents
        case getClientEvents
        
        var method: HTTPMethod {
            switch self {
            case .sendCreateEvent:
                return .post
            case .acceptEvent, .declineEvent, .confirmEvent:
                return .put
            default:
                return .get
            }
        }
        
        var path: String {
            switch self {
            case .sendCreateEvent:
                return "/api/events"
            case .confirmEvent(let id):
                return "/api/events/\(id)/confirm"
            case .acceptEvent(_, let id):
                return "/api/events/\(id)/accept"
            case .declineEvent(let id):
                return "/api/events/\(id)/decline"
            case .getOrganizerEvents:
                return "/api/events/organizer"
            case .getClientEvents:
                return "/api/events/client"
            }
        }
        
        public func asURLRequest() throws -> URLRequest {
            let url = try PrivateManagerRouter.baseURLString.asURL()
            var urlRequest = URLRequest(url: url.appendingPathComponent(path))
            
            urlRequest.httpMethod = method.rawValue
            urlRequest.setValue("application/json", forHTTPHeaderField: "Accept")
            urlRequest.setValue("application/json", forHTTPHeaderField: "Content-Type")
            urlRequest.setValue("Bearer \(UserAuthentication.getToken())", forHTTPHeaderField: "Authorization")
            
            
            switch self {
            case .sendCreateEvent(let parameters):
                return try Alamofire.JSONEncoding.default.encode(urlRequest, with: parameters.toJSON())
            case .acceptEvent(let parameters, _):
                return try Alamofire.JSONEncoding.default.encode(urlRequest, with: parameters.toJSON())
            case .getClientEvents:
                return try Alamofire.URLEncoding.default.encode(urlRequest, with: ["status": "accepted"])
            default:
                return urlRequest
            }
        }
    }
    
}

