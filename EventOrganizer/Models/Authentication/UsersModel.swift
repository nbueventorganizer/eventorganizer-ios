//
//  UsersModel.swift
//  EventOrganizer
//
//  Created by Alexander Karaatanasov on 13.01.19.
//  Copyright © 2019 Alexander Karaatanassov. All rights reserved.
//

import Foundation
import ObjectMapper

class UsersModel: ImmutableMappable {
    
    var users: [UserModel]
    
    required init(map: Map) throws {
        users = try map.value("users")
    }
    
    func mapping(map: Map) {
        users <- map["users"]
    }
    
    init(users: [UserModel]) {
        self.users = users
    }
}
