//
//  CustomTabBarController.swift
//  EventOrganizer
//
//  Created by Alexander Karaatanasov on 1.12.18.
//  Copyright © 2018 Alexander Karaatanassov. All rights reserved.
//

import UIKit

class CustomTabBarController: UITabBarController {
    
    private enum Constants {
        static let shadowRadius: CGFloat = 4.0
    }
    
    // MARK: - Lifecylce
    
    override func viewDidLoad() {
        super.viewDidLoad()
        removeBorder()
        addShadow(with: Constants.shadowRadius)
    }
    
    // MARK: - Private
    
    private func addShadow(with radius: CGFloat) {
        let tabBarController = self as UITabBarController
        let tabGradientView = UIView(frame: tabBarController.tabBar.bounds)
        
        tabGradientView.backgroundColor = UIColor.white
        tabGradientView.translatesAutoresizingMaskIntoConstraints = false
        tabGradientView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        tabGradientView.layer.shadowOffset = CGSize(width: 0, height: 0)
        tabGradientView.layer.shadowRadius = radius
        tabGradientView.layer.shadowColor = GlobalColor.shadow.cgColor
        tabGradientView.layer.shadowOpacity = 0.6
        
        tabBarController.tabBar.addSubview(tabGradientView)
        tabBarController.tabBar.sendSubviewToBack(tabGradientView)
        tabBarController.tabBar.clipsToBounds = false
    }
    
    private func removeBorder() {
        tabBar.backgroundImage = UIImage()
        tabBar.shadowImage = UIImage()
    }
}
