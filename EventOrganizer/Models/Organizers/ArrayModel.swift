//
//  ArrayModel.swift
//  EventOrganizer
//
//  Created by Alexander Karaatanasov on 28.01.19.
//  Copyright © 2019 Alexander Karaatanassov. All rights reserved.
//

import Foundation
import ObjectMapper

class ArrayModel: ImmutableMappable, Equatable {
    let id: Int
    
    required init(map: Map) throws {
        id = try map.value("id")
    }
    
    func mapping(map: Map) {
        id >>> map["id"]
    }
    
    static func == (lhs: ArrayModel, rhs: ArrayModel) -> Bool {
        return lhs.id == rhs.id
    }
}
