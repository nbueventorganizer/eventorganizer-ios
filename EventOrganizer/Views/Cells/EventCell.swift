//
//  EventCell.swift
//  EventOrganizer
//
//  Created by Alexander Karaatanasov on 13.01.19.
//  Copyright © 2019 Alexander Karaatanassov. All rights reserved.
//

import UIKit
import FoldingCell
import ObjectMapper
import SVProgressHUD

class EventCell: FoldingCell {
    
    var controllerInstance: EventsTableViewController?
    
    var number: Int = 0 {
        didSet {
            closeNumberLabel.text = String(number)
            openNumberLabel.text = String(number)
        }
    }
    var clientId: Int? = 0 { // make non-optional
        didSet {
            guard let clientId = clientId else {
                openClientNameLabel.text = "nil"
                return
            }
            openClientNameLabel.text = clientNameFrom(id: clientId)
        }
    }
    var organizerId: Int? = 0 { // make non-optional
        didSet {
            guard let organizerId = organizerId else {
                openOrganizerNameLabel.text = "nil"
                return
            }
            openOrganizerNameLabel.text = organizerNameFrom(id: organizerId)
        }
    }
    var typeId: Int? = 0 { // make non-optional
        didSet {
            guard let typeId = typeId else {
                closeTypeLabel.text = "nil"
                openTypeLabel.text = "nil"
                return
            }

            closeTypeLabel.text = typeNameFrom(id: typeId)
            openTypeLabel.text = typeNameFrom(id: typeId)
        }
    }
    var statusId: Int? = 0 { // make non-optional
        didSet {
            guard let statusId = statusId else {
                openStatusLabel.text = "nil"
                return
            }
            openStatusLabel.text = statusNameFrom(id: statusId)
        }
    }
    var eventTitle: String = "Title" {
        didSet {
            closeTitleLabel.text = eventTitle
            openTitleLabel.text = eventTitle
        }
    }
    var eventDescription: String = "Some\nDescription" {
        didSet {
            openDescriptionLabel.text = eventDescription
        }
    }
    var eventDate: String = "2019-01-15" {
        didSet {
            // TODO: - Make dates Date objects
            let dateParts = eventDate.split(separator: "-")
            let year = dateParts[0]
            let month = dateParts[1]
            let day = dateParts[2]
            closeFirstDateLabel.text = String(year)
            openSecondDateLabel.text = String(year)
            closeSecondDateLabel.text = String(month) + "-" + String(day)
            openFirstDateLabel.text = String(month) + "-" + String(day)
        }
    }
    var eventLocation: String = "EXE" {
        didSet {
            closeLocationLabel.text = eventLocation
        }
    }
    var feedingPrice: String = "10.00" {
        didSet {
            closeFeedingLabel.text = feedingPrice
            openFeedingLabel.text = feedingPrice
        }
    }
    var accommodationPrice: String = "10.00" {
        didSet {
            closeAccommodationLabel.text = accommodationPrice
            openAccommodationLabel.text = accommodationPrice
        }
    }
    var involvement: String = "Involved" {
        didSet {
            openInvolvedLabel.text = involvement
        }
    }
    var eventCreationDate: String = "2019-01-12 20:33:42" { // make non-string
        didSet {
            openCreatedOnLabel.text = "Created on: \(eventCreationDate)"
        }
    }
    var eventLastUpdate: String = "2019-01-12 20:33:42" { // make non-string
        didSet {
            openUpdatedOnLabel.text = "Last updated: \(eventLastUpdate)"
        }
    }
    
    // MARK: - IBOutlets
    
    // Closed
    @IBOutlet weak var closeNumberLabel: UILabel!
    @IBOutlet weak var closeFirstDateLabel: UILabel!
    @IBOutlet weak var closeSecondDateLabel: UILabel!
    
    @IBOutlet weak var closeTitleLabel: UILabel!
    @IBOutlet weak var closeLocationLabel: UILabel!
    
    @IBOutlet weak var closeTypeLabel: UILabel!
    @IBOutlet weak var closeFeedingLabel: UILabel!
    @IBOutlet weak var closeAccommodationLabel: UILabel!
    
    // Opened
    @IBOutlet weak var openNumberLabel: UILabel!
    @IBOutlet weak var openTitleLabel: UILabel!
    
    @IBOutlet weak var openTypeLabel: UILabel!
    @IBOutlet weak var openFeedingLabel: UILabel!
    @IBOutlet weak var openAccommodationLabel: UILabel!
    
    @IBOutlet weak var openClientNameLabel: UILabel!
    @IBOutlet weak var openCreatedOnLabel: UILabel!
    
    @IBOutlet weak var openOrganizerNameLabel: UILabel!
    @IBOutlet weak var openStatusLabel: UILabel!
    @IBOutlet weak var openInvolvedLabel: UILabel!
    
    @IBOutlet weak var openFirstDateLabel: UILabel!
    @IBOutlet weak var openSecondDateLabel: UILabel!
    @IBOutlet weak var openDescriptionLabel: UILabel!
    
    @IBOutlet weak var buttonStackView: UIStackView!
    @IBOutlet weak var openUpdatedOnLabel: UILabel!
    
    // MARK: - Lifecycle
    
    override func awakeFromNib() {
        foregroundView.layer.cornerRadius = 10
        foregroundView.layer.masksToBounds = true
        super.awakeFromNib()
    }
    
    override func animationDuration(_ itemIndex: NSInteger, type _: FoldingCell.AnimationType) -> TimeInterval {
        let durations = [0.26, 0.2, 0.2]
        return durations[itemIndex]
    }
    
    // MARK: - Private
    
    //    private func findNameById<T: ImmutableMappable>(in array: [T], with id: Int) -> String {
    //        for element in array {
    //            if element.id == id {
    //
    //            }
    //        }
    //    }
    
    private func clientNameFrom(id: Int) -> String {
        var clientName = "--"
        guard let clients = controllerInstance?.getUsers() else { return clientName }
        for client in clients {
            if client.id == id {
                clientName = client.name
                break
            }
        }
        return clientName
    }
    
    private func organizerNameFrom(id: Int) -> String {
        var organizerName = "--"
        guard let organizers = controllerInstance?.getOrganizers() else { return organizerName }
        for organizer in organizers {
            if organizer.id == id {
                organizerName = organizer.name
                break
            }
        }
        return organizerName
    }
    
    private func typeNameFrom(id: Int) -> String {
        var typeName = "--"
        guard let types = controllerInstance?.getTypes() else { return typeName }
        for type in types {
            if type.id == id {
                typeName = type.title
                break
            }
        }
        return typeName
    }
    
    private func statusNameFrom(id: Int) -> String {
        var statusName = "--"
        guard let statuses = controllerInstance?.getStatuses() else { return statusName }
        statusName = statuses[id-1]
        return statusName
    }
    
    // MARK: - Public
    
    func configure(from controller: EventsTableViewController, with event: EventModel, buttons hidden: Bool) {
        controllerInstance = controller
        number = event.id
        clientId = event.clientId
        organizerId = event.organizerId
        typeId = event.typeId
        statusId = event.statusId
        eventTitle = event.title
        eventDescription = event.description
        eventDate = event.date
        eventLocation = event.location ?? "Sofia"
        feedingPrice = event.feedingPrice ?? "20.00"
        accommodationPrice = event.accommodationPrice ?? "50.00"
        involvement = event.involved
        eventCreationDate = event.creationDate
        eventLastUpdate = event.lastUpdate
        buttonStackView.isHidden = hidden
    }
    
    // MARK: - IBActions
    
    @IBAction func barButtonHandler(_ sender: Any) {
        print("barButton")
    }
    
    @IBAction func leftButton(_ sender: Any) {
        SVProgressHUD.show()
        PrivateManagerAPI.sharedInstance.declineEvent(with: number, completionHandler: { (response, error) in
            SVProgressHUD.dismiss()
            if let errorModel = error {
                switch errorModel.error {
                case ErrorType.unauthorized.rawValue:
                    AlertPresenter.showInformationAlert(from: self.controllerInstance!, title: "Oops", message: "You must login before you can create events.")
                case ErrorType.unauthorized.rawValue:
                    AlertPresenter.showInformationAlert(from: self.controllerInstance!, title: "Oops", message: "Session expired. Please log in again.")
                default: break
                }
            } else {
                AlertPresenter.showInformationAlert(from: self.controllerInstance!,
                                                    title: "Success",
                                                    message: "This event was declined successfully.",
                                                    okAction: (target: self, actionSelector: #selector(self.dismissAndPopController)))
            }
        })
    }
    
    @objc private func dismissAndPopController() {
        controllerInstance?.dismiss(animated: true) { [weak self] in
            self?.controllerInstance?.reloadData()
        }
    }
    
    @IBAction func rightButton(_ sender: Any) {
        if controllerInstance?.eventsType == .clientEvents {
            SVProgressHUD.show()
            PrivateManagerAPI.sharedInstance.confirmEvent(with: number, completionHandler: { (response, error) in
                SVProgressHUD.dismiss()
                if let errorModel = error {
                    switch errorModel.error {
                    case ErrorType.unauthorized.rawValue:
                        AlertPresenter.showInformationAlert(from: self.controllerInstance!, title: "Oops", message: "You must login before you can create events.")
                    case ErrorType.unauthorized.rawValue:
                        AlertPresenter.showInformationAlert(from: self.controllerInstance!, title: "Oops", message: "Session expired. Please log in again.")
                    default: break
                    }
                } else {
                    AlertPresenter.showInformationAlert(from: self.controllerInstance!,
                                                        title: "Success",
                                                        message: "This event was created successfully.",
                                                        okAction: (target: self, actionSelector: #selector(self.dismissAndPopController)))
                }
            })
        } else {
            controllerInstance?.showCreateEvent()
        }
    }
    
}

// MARK: - Actions

extension EventCell {
    @IBAction func buttonHandler(_: AnyObject) {
        print("button")
    }
}
