//
//  OAuthHandler.swift
//  EventOrganizer
//
//  Created by Alexander Karaatanasov on 12.01.19.
//  Copyright © 2019 Alexander Karaatanassov. All rights reserved.
//

import Foundation
//import Alamofire
//import AlamofireObjectMapper
//import ObjectMapper
//import SwiftyUserDefaults
//
//fileprivate let refreshTokenErrorKey = "unauthenticated"
//
//class OAuthHandler: RequestAdapter, RequestRetrier {
//    private typealias RefreshCompletion = (_ succeeded: Bool, _ accessToken: String?, _ refreshToken: String?) -> Void
//    
//    private let lock = NSLock()
//    
//    private var baseURLString: String
//    
//    private var isRefreshing = false
//    private var requestsToRetry: [RequestRetryCompletion] = []
//    
//    // MARK: - Initialization
//    
//    public init(baseURLString: String) {
//        self.baseURLString = baseURLString
//    }
//    
//    // MARK: - RequestAdapter
//    
//    func adapt(_ urlRequest: URLRequest) throws -> URLRequest {
//        if let url = urlRequest.url,
//            url.relativeString.hasPrefix(baseURLString),
//            let tokenType = Defaults[.tokenType],
//            let accessToken = Defaults[.accessToken] {
//            var urlRequest = urlRequest
//            urlRequest.setValue("\(tokenType) \(accessToken)", forHTTPHeaderField: "Authorization")
//            return urlRequest
//        }
//        
//        return urlRequest
//    }
//    
//    func createError(_ failureError: Error, data: Data) -> ErrorModel {
//        var parsedError: ErrorModel
//        do {
//            let jsonObject = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? NSDictionary
//            parsedError = Mapper<ErrorModel>().map(JSONObject: jsonObject) ?? ErrorModel(failureError)
//        }
//        catch {
//            parsedError = ErrorModel(failureError)
//        }
//        
//        return parsedError
//    }
//    
//    // MARK: - RequestRetrier
//    
//    func should(_ manager: SessionManager,
//                retry request: Request,
//                with error: Error,
//                completion: @escaping RequestRetryCompletion) {
//        
//        lock.lock() ; defer { lock.unlock() }
//        
//        guard let request = request as? DataRequest, let data = request.delegate.data else {
//            completion(false, 0.0)
//            return
//        }
//        let parsedError = createError(error, data: data)
//        
//        guard let errorType = ErrorType(rawValue: parsedError.error) else {
//            completion(false, 0.0)
//            return
//        }
//        
//        switch errorType {
//        case .tokenExpired:
//            requestsToRetry.append(completion)
//            
//            if !isRefreshing {
//                refreshTokens { [weak self] succeeded, accessToken, refreshToken in
//                    
//                    guard let strongSelf = self else { return }
//                    
//                    strongSelf.lock.lock() ; defer { strongSelf.lock.unlock() }
//                    
//                    if !succeeded {
//                        strongSelf.requestsToRetry = []
//                        
//                        UserHelper.localLogout()
//                        completion(false, 0.0)
//                        return
//                    }
//                    
//                    if let accessToken = accessToken, let refreshToken = refreshToken {
//                        Defaults[.accessToken] = accessToken
//                        Defaults[.refreshToken] = refreshToken
//                    }
//                    
//                    strongSelf.requestsToRetry.forEach { $0(succeeded, 0.0) }
//                    strongSelf.requestsToRetry.removeAll()
//                }
//            }
//        default:
//            completion(false, 0.0)
//            return
//        }
//    }
//    
//    // MARK: - Private - Refresh Tokens
//    
//    private func refreshTokens(completion: @escaping RefreshCompletion) {
//        if isRefreshing { return }
//        
//        isRefreshing = true
//        
//        PublicManagerAPI.sharedInstance.refreshToken() { [weak self] (response, error) in
//            if let response = response, error == nil {
//                UserHelper.setToken(with: response)
//                completion(true, response.accessToken, response.refreshToken)
//            } else {
//                completion(false, nil, nil)
//            }
//            
//            self?.isRefreshing = false
//        }
//    }
//}
