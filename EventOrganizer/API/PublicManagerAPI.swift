//
//  PublicManagerAPI.swift
//  EventOrganizer
//
//  Created by Alexander Karaatanasov on 12.01.19.
//  Copyright © 2019 Alexander Karaatanassov. All rights reserved.
//

import Foundation
import Alamofire
import AlamofireObjectMapper
import SwiftyUserDefaults

class PublicManagerAPI: BaseManagerAPI {
    
    override init() {
        super.init()
    }
    
    class var sharedInstance: PublicManagerAPI {
        struct Static {
            static let instance = PublicManagerAPI()
        }
        
        return Static.instance
    }
    
//    func refreshToken(_ completionHandler: @escaping ((AccessTokenModel?, ErrorModel?) -> Void)) {
//        let accessToken = AccessTokenRequestModel(type: GrantType.refresh,
//                                                  clientId: AppConstant.clientId,
//                                                  clientSecret: AppConstant.clientSecret,
//                                                  refreshToken: Defaults[.refreshToken])
//
//        performObjectRequestForRefresh(PublicManagerRouter.login(with: accessToken),
//                                       completionHandler: completionHandler)
//    }

//    func validate(_ user: UserRegistrationModel, completionHandler: @escaping ((ErrorModel?) -> Void)) {
//        performRequest(PublicManagerRouter.validate(user: user), completionHandler: completionHandler)
//    }
    
     // - MARK: Requests
    
    func register(with register: RegisterModel, completionHandler: @escaping ((RegisterResponseModel?, ErrorModel?) -> Void)) {
        performObjectRequest(PublicManagerRouter.register(with: register), completionHandler: completionHandler)
    }
    
    func login(with login: LoginModel, completionHandler: @escaping ((ResponseTokenModel?, ErrorModel?) -> Void)) {
        performObjectRequest(PublicManagerRouter.login(with: login), completionHandler: completionHandler)
    }
    
    func getEvents(completionHandler: @escaping ((EventsModel?, ErrorModel?) -> Void)) {
        performObjectRequest(PublicManagerRouter.getEvents, completionHandler: completionHandler)
    }
    
    func getTypes(completionHandler: @escaping ((TypesModel?, ErrorModel?) -> Void)) {
        performObjectRequest(PublicManagerRouter.getTypes, completionHandler: completionHandler)
    }
    
    func getUsers(completionHandler: @escaping ((UsersModel?, ErrorModel?) -> Void)) {
        performObjectRequest(PublicManagerRouter.getUsers, completionHandler: completionHandler)
    }
    
    func getOrganizers(completionHandler: @escaping ((OrganizersModel?, ErrorModel?) -> Void)) {
        performObjectRequest(PublicManagerRouter.getOrganizers, completionHandler: completionHandler)
    }
    
//    func sendCreateEvent(with event: CreateEventModel, completionHandler: @escaping ((SuccessModel?, ErrorModel?) -> Void)) {
//        performObjectRequest(PublicManagerRouter.sendCreateEvent(with: event), completionHandler: completionHandler)
//    }
    
    // - MARK: Router
    
    private enum PublicManagerRouter: URLRequestConvertible {
        static let baseURLString = API.baseURL
        
        case register(with: RegisterModel)
        case login(with: LoginModel)

        case getEvents
        case getTypes
        case getUsers
        case getOrganizers
        
        var method: HTTPMethod {
            switch self {
            case .register, .login:
                return .post
            default:
                return .get
            }
        }
        
        var path: String {
            switch self {
            case .register:
                return "/api/users"
            case .login:
                return "/api/users/login"
            case .getEvents:
                return "/api/events"
            case .getTypes:
                return "/api/types"
            case .getOrganizers:
                return "/api/organizers"
            case .getUsers:
                return "/api/users"
            }
        }
        
        public func asURLRequest() throws -> URLRequest {
            let url = try PublicManagerRouter.baseURLString.asURL()
            var urlRequest = URLRequest(url: url.appendingPathComponent(path))
            
            urlRequest.httpMethod = method.rawValue
            urlRequest.setValue("application/json", forHTTPHeaderField: "Accept")
            urlRequest.setValue("application/json", forHTTPHeaderField: "Content-Type")
            
            switch self {
            case .register(let parameters):
                return try Alamofire.JSONEncoding.default.encode(urlRequest, with: parameters.toJSON())
            case .login(let parameters):
                return try Alamofire.JSONEncoding.default.encode(urlRequest, with: parameters.toJSON())
            default:
                return urlRequest
            }
        }
    }
}
