//
//  CustomAlertAction.swift
//  EventOrganizer
//
//  Created by Alexander Karaatanasov on 1.12.18.
//  Copyright © 2018 Alexander Karaatanassov. All rights reserved.
//

import Foundation
import UIKit

public class CustomAlertAction {
    public var title: String
    public var action: (target: NSObjectProtocol?, actionSelector: Selector)?
    
    public init(title: String, action: (target: NSObjectProtocol?, actionSelector: Selector)? = nil) {
        self.title = title
        self.action = action
    }
}
